require 'spec_helper'
require 'nokogiri'

# TODO add tests for Context widget

describe "gui_director" do
  before(:each) do
    clear_db
    @builder = double("builder")
    @director = Gui::Director.new(@builder)
    
    ChangeTracker.start
    @bike  = Automotive::Motorcycle.create(:make => 'Harley', :vehicle_model => '900')
    @rider = People::Person.create(:name => "Greg", :weight => '205.5')
    @bike.add_driver(@rider)
    @motorcycle = Automotive::Motorcycle.new(:make => 'Honda', :vehicle_model => 'Shadow')
    @person     = People::Person.new(:name => "Mark")
    ChangeTracker.commit
  end
  
  describe "rendering" do
    it "should be able to render the default domain object" do
      @builder.should_receive(:render).with(kind_of(Gui::Controller::Vertex), {})
      lambda { @director.render() }.should_not raise_error
    end
    
    it "should be able to render a domain object" do
      @builder.should_receive(:render).with(kind_of(Gui::Controller::Vertex), {})
      lambda { @director.render(@motorcycle) }.should_not raise_error
    end

    it "should be able to render a context" do
      pending("Fix this test when Contexts are worked out better")
      @builder.should_receive(:render).with(kind_of(Gui::Controller::Vertex), {})
      lambda { @director.render(@person, nil, :Context) }.should_not raise_error
    end
  end

  describe "vertex tree creation" do
    it "should be able to create a vertex tree based off of a domain object" do
      widget, parent_widget = @director.get_widget(@motorcycle)
      vertex = @director.build_vertex_tree(widget, nil, @motorcycle)
      vertex.should be_a(Gui::Controller::Vertex)
      vertex.domain_obj.should == @motorcycle
      vertex.widget.should be_a(Gui::Widgets::Organizer)
      # Check child vertices
      num_attrs_and_assocs = Automotive::Motorcycle.properties.count
      # Note that isn't always the case, since a custom view could define more or fewer widgets.
      vertex.children.count.should == num_attrs_and_assocs
      # Ensure that only the returned vertex is marked 'gui_root'
      vertex.gui_root.should be_truthy
      vertex.children.each { |child| child.gui_root.should be_falsey }
      # Ensure that the ViewRef widgets are hooked up to Views
      vr_children = vertex.children.select { |c| c.widget.is_a?(Gui::Widgets::ViewRef) }
      vr_children.each do |child|
        child.widget.view.should be_a(Gui::Widgets::View)
        # Ensure that exact view types are correct
        if [:drivers, :owners, :maintained_by, :components, :occupants].include?(child.widget.getter.to_sym)
          child.widget.view.should be_a(Gui::Widgets::Collection)
        elsif [:being_repaired_by, :vin, :warranty, :registered_at, :replacement_for].include?(child.widget.getter.to_sym)
          child.widget.view.should be_a(Gui::Widgets::Organizer)
        else
          raise "Unexpected association of Motorcycle class: #{child.widget.getter}"
        end
      end
      # Ensure that attribute domain_objs can be retreived
      vehicle_model_vertex = vertex.children.select { |c| c.widget.getter == 'vehicle_model' }.first
      vehicle_model_vertex.domain_obj.should == 'Shadow'
    end

    it "should be able to get domain objs for each vertex in the tree" do
      widget, parent_widget = @director.get_widget(@bike)
      vertex = @director.build_vertex_tree(widget, nil, @bike)
      driver_vertex = vertex.children.select { |cv| cv.widget.getter.to_sym == :drivers }.first
      # Get domain objs
      driver_vertex.domain_obj.should include(@rider)
      # Test caching by calling a second time
      driver_vertex.domain_obj.should include(@rider)

      # Get domain obj associations
      driver_vertex.domain_obj_associations.map { |h| h[:to] }.should include(@rider)
      # Test caching by calling a second time
      driver_vertex.domain_obj_associations.map { |h| h[:to] }.should include(@rider)
    end
    
  end
end

describe "gui_builders" do
  before(:each) do
    clear_db
  end
  
  describe "default builder" do
    it "should use the default html_gui_builder if none is specified" do
      @director = Gui::Director.new
      ChangeTracker.start
      @motorcycle = Automotive::Motorcycle.new(:make => 'Honda', :vehicle_model => 'Shadow')
      @person = People::Person.new(:name => "Mark")
      ChangeTracker.commit
      rendering = @director.render(@motorcycle)
      html_elements = html_elements_from_rendering(rendering)
      organizer = html_elements.first
      organizer.should have_class('organizer')
      organizer.should have_class('root')
    end
    
    it "should be able to render a collection" do
      director = Gui::Director.new
      ChangeTracker.start
      domain_obj = Automotive::Motorcycle.new(:make => 'Honda', :vehicle_model => 'Shadow')
      ChangeTracker.commit
      getter = :drivers
      rendering = director.render(domain_obj, getter)
      html_elements = html_elements_from_rendering(rendering)
      collection = html_elements.first
      collection.should have_class('collection')
    end
    
    it "should be able to render collection content" do
      
      director = Gui::Director.new
      ChangeTracker.start
      domain_obj = Automotive::Motorcycle.new(:make => 'Honda', :vehicle_model => 'Shadow')
      ChangeTracker.commit
      getter = :drivers
      rendering = director.render_collection_content(domain_obj, getter)
      html_elements = html_elements_from_rendering(rendering)
      # TODO: fix it so this is accurate
      #html_elements.count.should == 1 # The collection_content_data div
      collection_content_data = html_elements.first
      collection_content_data.should have_class('collection_content_data')
    end
    
    it "should be able to render a collection with unassociated items already showing" do
      # This specific example is used when rendering a clone_popup to select an existing composer
      director = Gui::Director.new
      ChangeTracker.start
      domain_obj = Automotive::Component.new
      ChangeTracker.commit
      getter = :vehicle
      options = render_options = {
        :unassociated_only => true,
        :expanded => true,
        :hide_collection_header => true,
        :selection => true,
        :selection_param_name => 'composer_option'
      }
      rendering = director.render(domain_obj, getter, nil, nil, nil, options)
      html_elements = html_elements_from_rendering(rendering)
      collection = html_elements.first
      collection.should have_class('collection')
      collection.should have_class('expanded')
      header = collection.css('.collection_header').first
      header.should have_class('composer')
      header.should have_class('hidden')
      content = collection.css('.collection_content').first
      content.should have_class('view_content')
    end
    
    it "should be able to render a simple widget" do
      director = Gui::Director.new
      ChangeTracker.start
      domain_obj = People::Person.new
      ChangeTracker.commit
      getter = :name
      rendering = director.render(domain_obj, getter)
      rendering.should =~ /^<div class='mutable simple_widget string widget'/
    end
    
    describe "Template rendering" do
      it "should be able to render a template for an organizer" do
        @director = Gui::Director.new
        template = @director.render_template(Automotive::Motorcycle, nil, :Organizer, :Details, Automotive::Motorcycle, {})
        #html_elements = html_elements_from_rendering(rendering)
        # puts template
        # organizer.should have_class('organizer')
        # organizer.should have_class('root')
        # File.open('motorcycle.haml', 'w+') { |f| f.puts template }
      end
      
      it "should be able to render a template for a collection" do
        director = Gui::Director.new
        getter = :drivers
        template = director.render_template(Automotive::Motorcycle, getter, :Collection, :Summary, Automotive::Motorcycle, {})
        # puts template
      end
      
      it "should be able to render a template for collection content" do
        director = Gui::Director.new
        getter = :drivers
        template = director.render_template(Automotive::Motorcycle, getter, :Collection, :Summary, Automotive::Motorcycle, :collection_content => true)
        # puts template
      end
    end
  end
end
