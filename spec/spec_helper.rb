require 'rspec'
require 'lodepath'
LodePath.amend
LodePath.display

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end
require 'gui_director'

require 'sequel'
require 'sequel_change_tracker'

# Put local car_example folder on load path
require 'car_example'
require LodePath.find('car_example/meta_info.rb')
options = CarExample::DEFAULT_APP_OPTIONS
# Inject Prometheus folder options into default_app_options
data_dir = File.expand_path('~/Prometheus/data/car_example_generated')
options[:data_dir]     = data_dir
options[:log_dir]      = File.join(data_dir, 'logs')
options[:tempfile_dir] = File.join(data_dir, 'tmp')

Gui.configure_director(options)

RSpec::Matchers.define :have_class do |expected|
  match do |actual|
    raise "Don't know how to get CSS classes for: #{actual.class}" unless actual.is_a?(Nokogiri::XML::Element)
    actual['class'].split(' ').include?(expected)
  end
end

def html_elements_from_rendering(rendering)
  raise "Rendering must be a non-empty string" unless rendering.is_a?(String) && !rendering.empty?
  # Nokogiri fragments are unusable since they do not correctly define a namespace (or the absence of one)
  #html_elements = Nokogiri::HTML::DocumentFragment.parse(rendering).children
  # For now, using a full HTML document to parse the fragment. This means that the we must manually
  # remove the <html> and <body> tags added by nokogiri's html parser.
  mangled_html = Nokogiri::HTML(rendering)
  html_elements = mangled_html.children[1].css('body').children
end

def clear_db
  DB.tables.each do |table|
    DB[table].delete
  end
end