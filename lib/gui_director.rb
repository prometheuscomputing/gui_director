require 'ramaze'
module Gui
  WebFrameworkController = Ramaze::Controller
  class Controller < WebFrameworkController
  end
end

require 'rainbow'
require 'active_support/core_ext/string/filters'
require 'active_support/core_ext/object/blank'
require 'active_support/core_ext/class/subclasses' # Class#descendants & Class#subclasses

# require 'Foundation/load_path_management' # FIXME make this go away
require 'gui_director/meta_info'
require 'gui_director/utils'
require 'gui_director/options'
require 'gui_director/search'
require 'gui_director/class_table_map'
require_dir(relative('gui_director/ruby_extensions'))
# Setup facility for creating pages that are not based on domain objects but that have some of the behaviors of domain objects
require 'gui_director/page_object/setup'
# Require Spec (Used to store loaded DSL widgets)
require 'gui_spec'
require 'gui_director/policy'
require_dir(relative('gui_director/director'))
require_dir(relative('gui_director/vertex'))
require 'gui_director/gui_builder'
require 'gui_director/errors'
require 'gui_director/resolvable'
require 'gui_director/director_config'
require 'gui_director/track_option_use' # remove after refactor
