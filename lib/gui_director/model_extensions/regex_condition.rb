module Gui_Builder_Profile
  class RegularExpressionCondition
    def matches?(string)
      !(self.regular_expression.to_full_regexp =~ string).nil?
    end
    def regular_expression=(value)
      case value
      when String
        regexp = value.to_full_regexp
        self[:regular_expression] = regexp.to_string
      when Regexp
        self[:regular_expression] = value.to_string
      else
        raise "Value of invalid type: '#{value.class}' assigned as regular expression."
      end
    end
  end
end
