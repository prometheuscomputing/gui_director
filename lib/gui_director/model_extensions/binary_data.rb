module Gui_Builder_Profile
  class BinaryData
    # TODO: This definition may be unnecessary -SD
    def eql?(other)
      return false unless other && other.is_a?(BinaryData)
      self.data == other.data
    end
  end
end
