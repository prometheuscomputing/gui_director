module Gui
  module Utils
    module_function
    
    # Trims off the leading and trailing newline
    # Necessary for Haml bug that eats 1 trailing newline if any,
    # and for HTML bug (in chrome and firefox) that eats 1 leading newline.
    # TODO: Make more efficient somehow. Can't use regex ^$ operators on multiline strings.
    # WARNING: this method will not do what you want it to if you call #remove_carriage_returns before this
    def trim_leading_and_trailing_newlines(value)
      return value unless value.is_a?(::String)
      value.sub(/\A(\r|\n)+/m, '').sub(/(\r|\n)+\z/m, '')
    end
    
    # Remove all carriage returns
    # Browsers insert carriage returns wherever newlines are in text areas.
    # Need to remove them all, even from the database (from data imports and for older database compatibility)
    def remove_carriage_returns(value)
      return value unless value.is_a?(::String)
      value.gsub(/\r/, '')
    end
  end
end

module GuiDirector
  
  # support backwards compatibility in case any existing model extensions call #administrator_do
  def self.administrator_do(&block)
    block.call
  end
  
  def self.start_change_tracker
    ChangeTracker.start if change_tracker_is_defined?
  end

  def self.commit_change_tracker(&block)
    return unless change_tracker_is_defined?
    ChangeTracker.commit(&block)
  end
  
  def self.clear_change_tracker
    ChangeTracker.clear_changes if change_tracker_is_started?
  end
  
  def self.change_tracker_is_defined?
    defined?(ChangeTracker)
  end
  
  def self.change_tracker_is_started?
    return false unless change_tracker_is_defined?
    ChangeTracker.started?
  end
end
