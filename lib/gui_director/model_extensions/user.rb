module Gui_Builder_Profile
  class User
    plugin :validation_helpers
    plugin :boolean_readers
    
    USERNAME_MIN = 3
    USERNAME_MAX = 32
    PASSWORD_MIN = 8
    PASSWORD_MAX = 256
    
    def admin?
      self.roles.any?(&:admin?)
    end
    
    # Register a new user locally
    # NOTE: This occurs when a user registers via an invitation code.
    def self.register(user_info)
      return nil unless LOCAL_AUTHENTICATION
      # Get the invitation code from the passed code
      unless user_info[:is_admin_panel]
        user_info[:invitation_code] = Gui_Builder_Profile::InvitationCode[:code => user_info[:code]]
      end
      error_messages = User.validate(user_info.merge(:is_registration => true))
      # Retrieve (creating if necessary) the admin role
      admin_role = Gui_Builder_Profile::UserRole.admin
      raise Gui::InvalidLogin, error_messages.join("\n") if error_messages.any?
      if user_info[:first_user]
        roles_for_user = [admin_role]
        perspectives_for_user = []
      elsif user_info[:is_admin_panel] # Registration from organization administration panel
        roles_for_user = []
        perspectives_for_user = []
      
      # FIXME this looks all weird and there probably are no tests!
      elsif Gui.option(:enable_registration_without_inv_code) && !user_info[:invitation_code] #Registration without requiring and invitation code if the option is enabled
        roles_for_user = []
        perspectives_for_user = []
        if defined?(ChangeTracker) && ChangeTracker.started?
          ChangeTracker.commit
          ChangeTracker.start
        end
      else
        # roles_for_user = nil # why? if 2 lines later we are setting this...
        # perspectives_for_user = nil
        roles_for_user        = user_info[:invitation_code]&.roles_granted
        perspectives_for_user = user_info[:invitation_code]&.perspectives
        unless user_info[:invitation_code].uses_remaining.nil? || user_info[:invitation_code].uses_remaining < 0
          user_info[:invitation_code].uses_remaining -= 1
          user_info[:invitation_code].save
        end
      end
      roles_for_user += user_info[:roles] if user_info[:roles]
      perspectives_for_user += user_info[:perspectives] if user_info[:perspectives]
      
      new_user = Gui_Builder_Profile::User.new_user(user_info)
      if defined?(ChangeTracker) && ChangeTracker.started?
        ChangeTracker.commit
        ChangeTracker.start
      end
      # This must occur after organizations have been set, since role addition hooks apply organization permissions
      new_user.roles = roles_for_user
      new_user.perspectives = perspectives_for_user
      add_user_to_organization(new_user, user_info[:organization])

      new_user
    end

    def self.new_user(user_info)
      user = Gui_Builder_Profile::User.new
      user.first_name = user_info[:first_name]
      user.last_name  = user_info[:last_name]
      user.login      = user_info[:login]
      user.email      = user_info[:email]
      user.change_password(user_info[:password])
      user.email_confirmation_token = user.generate_token
      user.add_perspective(Gui_Builder_Profile::Perspective.default_perspective)
      user.use_accessibility = false
      user.save
    end
    
    def generate_token
      SecureRandom.urlsafe_base64(32, true)
    end
    
    def verify_email
      self.email_confirmation_token = nil
      self.email_verified = true
      save
    end
    
    def change_email(email)
      self.email = email
      self.email_confirmation_token = self.generate_token
      self.email_verified = false
      self.save
    end
    
    def generate_password_reset_info
      self.password_reset_token = self.generate_token
      self.password_reset_time_limit = ::Time.now + (7*24*60*60)
      save
    end
    
    def change_password(new_password)
      self.salt = String.random
      self.password_hash = Digest::SHA512.hexdigest(new_password + self.salt)
      save
    end
    
    def reset_password(new_password)
      self.invalidate_reset_token
      self.change_password(new_password)
    end
    
    def invalidate_reset_token
      self.password_reset_time_limit = ::Time.now - 7
      self.password_reset_token = nil
      save
    end
    
    def right_password?(password)
      (self.password_hash == Digest::SHA512.hexdigest(password + self.salt))
    end
    
    # This is called by Ramaze::Helper::UserHelper
    def self.authenticate(creds)
      return nil unless creds['login'] && creds['password']
      
      if LOCAL_AUTHENTICATION
        puts "running local authentication" if $verbose
        user_record = User[:login => creds['login']]
        locally_authenticated = false
        if user_record && creds['password']
          locally_authenticated = user_record.right_password?(creds['password'])
        end
        
        if locally_authenticated
          return user_record
        else
          # Validate here to tell the user if they entered an invalid password (doesn't match password constraints).
          # It won't stop them from logging in with a bad password (if that is indeed their password)
          error_messages = User.validate(:login => creds['login'], :password => creds['password'], :is_login => true)
          raise Gui::InvalidLogin, error_messages.join("\n") if error_messages.any?
        end
      end

      return nil
    end
    
    def self.valid_password_chars?(text)
      # limit to printable ASCII characters 32-126
      text =~ /^[\w @#*!%^&(){}\[\]:;"'`<>,.?~\/+-=|\\]+$/
    end
    
    def self.password_does_not_include_username?(text, username)
      raise 'Username is nil!' unless username
      text !~ /#{username}/i
    end
    

    def self.valid_username?(text)
      text =~ /^[A-Za-z0-9\_\-]+$/
    end

    def self.valid_role?(text)
      text =~ /^[A-Za-z0-9\_\-\ ]+$/
    end
    def self.valid_perspective?(text)
      self.valid_role?(text)
    end
    def self.valid_word?(text)
      text =~ /^[A-Za-z0-9\_\.\-\:\ ]+$/
    end

    def self.sanitize_int(num)
      num.to_i
    end
    
    # Validate login information
    # Note: this methoad assumes that either ChangeTracker is started, or that 
    #       ProjectOptions has already been instantiated
    def self.validate(user_info)
      errors = []
      project_options       = user_info[:project_options] || Gui_Builder_Profile::ProjectOptions.instance
      email_requirements    = user_info[:email_requirements] || project_options.email_requirements
      email_requirements   += user_info[:invitation_code].additional_email_requirements if user_info[:invitation_code]
      password_requirements = user_info[:password_requirements] || project_options.password_requirements
      
      # Validate password(s)
      if user_info[:is_registration] && !user_info[:is_admin_panel]
        errors += validate_passwords(user_info[:password], user_info[:reenter_password], user_info[:login], password_requirements)
      else
        errors += validate_password(user_info[:password], user_info[:login], password_requirements)
      end
      
      # Validate username
      errors += validate_username(user_info[:login], user_info[:is_registration])
      
      # Validate email if passed
      errors += validate_email(user_info[:email], email_requirements) unless user_info[:is_login]
        
      # Check invitation code on registration unless first user or registering from admin panel
      if user_info[:is_registration] && !user_info[:is_admin_panel] && !user_info[:first_user]
        errors += validate_invitation_code(user_info[:invitation_code], user_info[:code])
      end
      
      errors += validate_user_organization(user_info[:organization]) if (user_info[:is_registration] && Gui.option(:registration_requires_organization)) || user_info[:organization]
      
      errors
    end
    
    def self.validate_passwords(password, reenter_password, username, password_requirements = nil)
      password_requirements ||= Gui_Builder_Profile::ProjectOptions.instance.password_requirements
      errors = []
      errors += validate_password(password, username, password_requirements)
      # errors += validate_password(reenter_password, username, password_requirements)
      errors << "Entered passwords did not match" unless password == reenter_password
      errors.uniq
    end
    
    def self.valid_password_chars_message
      %q{Passwords can only contain letters, numbers, space (no tabs) and the following symbols: @#*!%^&(){}\[\]:;"'`<>,.?~\/+-=|}
    end
    
    def self.validate_password(password, username, password_requirements = nil)
      password_requirements ||= Gui_Builder_Profile::ProjectOptions.instance.password_requirements
      errors = []
      errors << "Password must be at least #{PASSWORD_MIN} characters" if password.length < PASSWORD_MIN
      errors << "Password must be no more than #{PASSWORD_MAX} characters" if password.length > PASSWORD_MAX
      errors << valid_password_chars_message unless valid_password_chars?(password)
      errors << "Password must not include your login name \"#{username}\" (regardless of capitalization)." unless password_does_not_include_username?(password, username)
      # Check that all custom password requirements are met
      password_requirements.each do |password_req|
        unless password_req.matches?(password)
          message = password_req.failure_message || "The password you entered failed to match: #{password_req.regular_expression}"
          message << " (#{password_req.description})" if password_req.failure_message.nil? && password_req.description
          errors << message
        end
      end
      errors
    end
    
    # Check username against custom requirements and length/character restrictions
    # If this is a new user, check that the username is not in use
    def self.validate_username(username, new_user = true)
      errors = []
      errors << "Username already exists. Please choose another." if new_user && Gui_Builder_Profile::User[:login => username]
      errors << "Username must be at least #{USERNAME_MIN} characters." if username.length < USERNAME_MIN
      errors << "Username must be no longer than #{USERNAME_MAX} characters." if username.length > USERNAME_MAX
      errors << "Usernames can only contain letters, numbers, underscores, and hyphens." unless valid_username?(username)
      errors
    end
    
    def self.validate_email(email, email_requirements, current_user_email = nil)
      errors = []
      # Check that all custom email requirements are met
      email_requirements.each do |email_req|
        unless email_req.matches?(email)
          message = email_req.failure_message || "The E-mail address you entered failed to match: #{email_req.regular_expression}"
          message << " (#{email_req.description})" if email_req.failure_message.nil? && email_req.description
          errors << message
        end
      end
      
      if Gui.option(:require_email_for_user)
        if !email || email == ""
          errors << "The E-mail address is required."
        end
      end
      
      if email != ""
        if (!current_user_email || current_user_email != email) && Gui_Builder_Profile::User[:email => email]
          errors << "This E-mail address is already in use. Please login to the account registered with #{email} or use a different E-mail address.  If you have forgotten your login credentials please use the 'Login' link and then follow the links to recover your username and/or password."
        end
      end
      
      errors
    end
    
    def self.validate_invitation_code(invitation_code, entered_code_string)
      errors = []
      if invitation_code.nil?
        return errors if entered_code_string.empty? && Gui.option(:enable_registration_without_inv_code)
        errors << "The invitation code you entered is not valid"
      elsif !(invitation_code.expires.nil? || invitation_code.expires > ::Time.now) # Check if code has expired
        errors << "The invitation code you entered has expired"
      elsif (invitation_code.uses_remaining == 0) # Check if code is depleted
        errors << "The invitation code you entered has exceeded the maximum number of uses"
      end
      errors
    end
    
    # For now we are just passing a string.  In the future, we may pass an Organizatoin object or something else.
    def self.validate_user_organization(org)
      puts Rainbow(org).orange
      if Gui.option(:registration_requires_organization)
        org.to_s.strip.empty? ? ['You must supply the name of an Organization during registration'] : []
      else
        []
      end
    end
    
    def self.add_user_to_organization(user, org)
      puts Rainbow(org).green
      return unless org
      if org.is_a?(String)
        org_obj = Gui_Builder_Profile::Organization.where(:name => org).first
        unless org_obj
          org_obj = Gui_Builder_Profile::Organization.new
          org_obj.name = org
          org_obj.save
        end
        org_obj.add_person(user)
      else
        raise "Unimplemented case: param :org is not a String"
      end
    end
  end
end
