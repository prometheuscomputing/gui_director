module Gui_Builder_Profile
  # Make ProjectOptions a singleton and define additional methods
  class ProjectOptions
    singleton! #TODO: Need to have generators handle this.
    # Shortcut to wrap (possible) instance creation with ChangeTracker transaction
    # FIXME assumes that we are using ChangeTracker!  Bad, Bad, Bad!!!
    def self.ct_instance
      ChangeTracker.start unless ChangeTracker.started?
      i = self.instance
      ChangeTracker.commit
      i
    end
  end
end
