module Gui_Builder_Profile
  class RichText
    # Setup multivocabulary
    if Gui.option(:multivocabulary)
      # Include Innate::Trinity in order to gain access to session (and request and reponse)
      include Innate::Trinity if defined?(Ramaze)
      attr_accessor :replacements
      def on_retrieve(retrieved_from)
        self[:content] = Multivocabulary.apply_substitutions(self[:content], session ? session[:substitutions] : [])
      end
      def on_assign(assigned_to)
        self[:content] = Multivocabulary.unapply_substitutions(self[:content], session ? session[:substitutions] : [])
      end
    end
  end
end
