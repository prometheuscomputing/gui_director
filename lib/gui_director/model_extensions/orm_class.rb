class ORM_Class
  # override this
  def page_title
    to_title
  end
  
  # override this  
  def to_title
    self.class.to_title
  end
  
  # override this  
  def collection_nav_url
    nil
  end
  
  # override this if you want something else
  def html_target
    '_self'
  end
end
