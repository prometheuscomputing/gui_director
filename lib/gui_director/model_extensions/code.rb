module Gui_Builder_Profile  
  class Code
    def eql?(code)
      return false unless code && code.is_a?(Code)
      if self.content == code.content && self.language == code.language
        true
      else
        false
      end    
    end
  end
end
