module Gui_Builder_Profile
  class MultivocabularySubstitution
    
    def shallow_dup
      values = {}
      values[:master_word] = self.master_word
      values[:replacement_word] = self.replacement_word
      
      self.class.create(values)
    end
  end
end
