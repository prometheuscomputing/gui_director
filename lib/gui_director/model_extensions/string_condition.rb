module Gui_Builder_Profile
  # Define additional methods on condition types
  class StringCondition
    def matches?(string); raise "Must override this method in subclasses of StringCondition"; end
  end
end
