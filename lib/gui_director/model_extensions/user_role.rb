module Gui_Builder_Profile
  class UserRole
    is_unique :name
    ADMIN = 'Administrator'
    LEGACY_ADMIN = 'Site Administrator'
    
    add_association_options(:users)
    
    # Retain backwards compatibility by looking for 'Site Administrator' role as well
    def admin?
      self.name == ADMIN || self.name == LEGACY_ADMIN
    end
    
    # Override == to prevent comparison of 'created_at' or 'updated_at' timestamps.
    # Only the role name is important for comparison
    def ==(other_role)
      other_role.is_a?(UserRole) && self.name == other_role.name
    end
      
    def self.admin
      @admin ||= find_or_create(:name => ADMIN)
    end
    def self.setup_roles
      admin
    end
  end
end
