module Gui_Builder_Profile
  # Widget convenience methods
  class File
    attr_accessor :temporary
    
    add_association_options(:binary_data, :composition => true)
    def data=(data_string)
      existing_binary_data = self.binary_data
      if existing_binary_data
        return if existing_binary_data.data == data_string
        existing_binary_data.data = data_string
        existing_binary_data.save
      else # No existing binary_data (Gui_Builder_Profile::BinaryData)
        if @temporary
          new_binary_data = Gui_Builder_Profile::BinaryData.new(:data => data_string)
        else
          new_binary_data = Gui_Builder_Profile::BinaryData.create(:data => data_string)
        end
        self.binary_data = new_binary_data
      end
    end
    
    # TODO we can speed this up by deleting the old BinaryData in #data= instead of reusing it, allowing us to avoid BLOB comparison.
    def eql? other
      attributes_are_equal = super
      return false unless attributes_are_equal
      obd = other.binary_data
      sbd = self.binary_data
      # There is a case where a file has become corrupt and is no longer associated with a binary data.  In this case, the files aren't equal (unless both are corrupt??)
      return true if obd.nil? && sbd.nil? # both are corrupt.  let us hope this doesn't happen
      return false unless obd && sbd
      obd.data == sbd.data
    end
    
    def data
      existing_binary_data = self.binary_data
      if existing_binary_data
        existing_binary_data.data
      else
        nil
      end
    end
    
    # Can't delete/destroy normally since File is not persisted
    def delete
      bd = self.binary_data
      bd.destroy if bd
      self
    end
    alias destroy delete
    
    # Create a File from a local path
    def self.from_path(path, options = {})
      expanded_path = ::File.expand_path(path)
      filename = ::File.basename(expanded_path)
      mime_type = MIME::Types.type_for(expanded_path).first
      mime_type_string = mime_type.content_type if mime_type
      data_string = ::File.binread(expanded_path)
      file = Gui_Builder_Profile::File.new(:filename => filename, :mime_type => mime_type_string)
      file.temporary = options[:temporary]
      file.data = data_string
      file
    end
  end
end
