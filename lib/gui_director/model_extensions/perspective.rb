module Gui_Builder_Profile 
  class Perspective
    class << self
      
      attr_accessor :default_perspective
      
      def new_perspective(name, base_perspective_id = nil)
        new_perspective = self.create(:name => name)
        if base_perspective_id
          base_perspective = self[base_perspective_id]
          base_perspective.substitutions.each do |sub|
            dup = sub.shallow_dup
            new_perspective.add_substitution(dup).save
          end
        end
        new_perspective
      end
    
      def setup_perspectives
        @default_perspective = self[:name => 'none'] || new_perspective('none')
      end
    end
  end
end
