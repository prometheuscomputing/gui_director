module Gui
  # Error indicating that some page content has triggered a failure.
  # TODO PageContentError should be moved to gui_site.  The idea of 'page' is specific to browser based applications.
  class PageContentError < StandardError
  end
  class LatexError < StandardError
  end  
  class ImageError < StandardError
  end
  class FileMissingError < StandardError
  end
  class WidgetValueError < StandardError
  end
  class ConcurrencyError < StandardError
    attr_reader :message, :concurrency_issues
    # attr_reader :messages, :warnings

    def initialize(concurrency_issues)
      # @warnings = concurrency_issues['warnings']
      # @messages = concurrency_issues['messages']
      @message = 'Data have been concurrently modified in another tab or by another user. Please resolve the following conflicts:'
      @concurrency_issues = concurrency_issues
    end
  end
  InvalidLogin    = Class.new(Exception)
  InvalidUsername = Class.new(InvalidLogin)
  InvalidPassword = Class.new(InvalidLogin)
end