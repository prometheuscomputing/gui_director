require_relative 'base_policy'
module Gui
  module_function
  def bootstrap_policy(klass)
    # puts "Define policy for #{klass}"
    # silly but fun way to do it as a one-liner...
    klass_name, mod_name = klass.name.reverse.split('::', 2).map(&:reverse)
    mod = Object.const_get(mod_name)
    policy_class = mod.const_set("#{klass_name}Policy", Class.new(BasePolicy))
    scope_class  = policy_class.const_set('Scope', Class.new(BasePolicy::Scope))
    klass.const_set('POLICY', policy_class)
    klass.const_set('SCOPE', scope_class)
    klass.define_singleton_method(:policy) { klass::POLICY }
    klass.define_singleton_method(:scope)  { klass::SCOPE }
  end


  def create_policy_classes
    ORM_Class.children.each do |klass|
      next unless klass.plugins.include?(Sequel::Plugins::SpecificAssociations)
      next if klass.associates? && !(klass.association_class?)
      bootstrap_policy(klass)
    end
    Gui::AdHocPageObject.descendants.each { |klass| bootstrap_policy(klass) }
  end
end
