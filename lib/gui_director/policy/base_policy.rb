class BasePolicy
  attr_reader :user, :obj
  def self.default
    @default ||= Gui.option(:policy_default)
  end
  
  def initialize(user, obj)
    @user = user
    @obj  = obj
  end

  def index?
    return true if user.admin?
    case BasePolicy.default
    when :read
      true
    when :write
      true
    else
      user.admin?
    end
  end

  def show?
    return true if user.admin?
    case BasePolicy.default
    when :read
      true
    when :write
      true
    else
      false
    end
  end

  def create?
    return true if user.admin?
    case BasePolicy.default
    when :read
      false
    when :write
      true
    else
      false
    end
  end

  # def new?
  #   create?
  # end

  def update?
    return true if user.admin?
    case BasePolicy.default
    when :read
      false
    when :write
      true
    else
      false
    end
  end

  def navigate?
    return true if user.admin?
    case BasePolicy.default
    when :read
      true
    when :write
      true
    else
      false
    end
  end
  # def edit?
  #   update?
  # end

  def destroy?
    return true if user.admin?
    case BasePolicy.default
    when :read
      false
    when :write
      true
    else
      false
    end
  end

  # scope is only for retrieval of existing objects
  # In our usage, resolve should always return a dataset or a proc (ability to handle a proc is not yet implemented though)
  class Scope
    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      return scope if user.admin?
      case BasePolicy.default
      when :read
        scope.dataset
      when :write
        scope.dataset
      else
        nil
      end
    end

    private
    attr_reader :user, :scope
  end
end
