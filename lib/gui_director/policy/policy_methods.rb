module Gui
  module PolicyMethods    
    def self.included(klass)
      klass.include Pundit
    end
    
    def current_user
      raise "#{self.class}#current_user must be defined by the class that includes Gui::PolicyMethods"
    end
    
    def authorize_creation(klass)
      return klass unless Gui.policy_active?
      authorize(klass, :create?)
      klass
    end
    
    # FIXME it is far more complicated than this...What if the current view of the object is restricted in scope by policy?  Should we only clone what the user can see? Cloning things that the user can't see would be disastrous
    def authorize_clone(obj)
      return obj unless Gui.policy_active?
      authorize(obj.class, :create?)
      obj
    end
    
    def authorize_object(obj, action)
      return obj unless Gui.policy_active?
      current_user.roles.each do |role|
        # TODO we probably need something more robust that role.name.to_sym
        # we just need to find a single role that has permission.
        # is this gross?
        begin
          authorize([role.name.to_sym, obj], action)
          return obj
        rescue
          next
        end
      end
      # FIXME return obj if the default is permissive.  fail if default behavior is not permissive.  perhaps give everyone a default / ghost role that causes failure?
      obj
    end
    
    def authorize_object_read(obj)
      authorize_object(obj, :read)
    end
    
    def class_scopes(klass)
      scopes = {}
      klass.descendants.unshift(klass).reject { |c| c.abstract? }.each do |c|
        s = policy_scope(c)
        scopes[c] = s if s
      end
      scopes
    end
    private :class_scopes
    
    
    # takes an obj and injects scope into method call
    # FIXME finish this
    def _get_authorized(method, obj, getter, opts = {})
      # puts Rainbow("#{method}: #{getter.inspect} -- #{opts.inspect}").orange
      return obj.send(method, **opts.merge({getter:getter})) unless Gui.policy_active?
      info = obj.class.get_info(getter)
      
      if info[:attribute] || info[:enumeration] || info[:primitive]
        # TODO implement stuff here...including complex attribute BS
        obj.send(method, **opts.merge({getter:getter}))
      else # association
        return obj.send(method, **opts.merge({getter:getter})) if info[:primitive] # and FIXME too...
        
        # puts Rainbow("#{obj.class} info for #{getter}:").orange
        # pp info
        scopes = class_scopes(info[:class].to_const)
        # puts Rainbow(scopes.inspect).cadetblue
        # puts
      
        if scopes.any? { |s| s.is_a?(Proc) }
          raise "#get_authorized_property not implemented to work with Proc based scope!"
          # The proc expects current user and an array of objects (TODO or maybe the proc expects a dataset?)
          # TODO do stuff
        else
          obj.send(method, **opts.merge({getter:getter, scope:scopes}))
        end
        
      end
    end

    def get_authorized_unassociated(obj, getter, opts = {})
      getter = getter.sub(/_unassociated$/, '')
      _get_authorized(:_get_unassociated, obj, getter, opts)
    end
    
    def get_authorized_property(obj, getter, opts = {})
      return get_authorized_unassociated(obj, getter, opts) if getter =~ /_unassociated$/
      _get_authorized(:_get_property, obj, getter, opts)
    end
    
    def authorize_object_write(obj)
      authorize_object(obj, :write)
    end
    
    # TODO implement me
    def authorized_attribute_read(obj, getter)
      return obj&.send(getter) unless Gui.policy_active?
      # authorize_object(obj, :read) # assume object already authorized?
      # authorize user's ability to read attributes of this object

      value = obj&.send(getter)
            # puts Rainbow("#{obj.class}.#{getter} = #{value}").orange
            # value
      # authorize user's ability to read attribute of this object with this value?
    end
    
    # TODO implement me
    def authorized_attribute_write(obj, getter, value)
      raise "don't even use this yet"
      return obj unless Gui.policy_active?
      obj
      # authorize_object(obj, :write) # assume object already authorized?
    end
    
    def authorize_association(obj1, obj2)
      return [obj1, obj2] unless Gui.policy_active?
      authorize_object_write(obj1)
      authorize_object_write(obj2)
      [obj1, obj2]
    end
    
    # def authorize_attribute_read(obj, getter)
    #   getter = getter.to_sym
    #   return unless Gui.policy_active?
    #   # FIXME implement me
    #   true
    # end
    #
    # def authorize_attribute_write(obj, getter, value)
    #   getter = getter.to_sym
    #   return unless Gui.policy_active?
    #   # FIXME implement me
    #   true
    # end
    
    # when fetching an object by id
    def object_in_policy_scope(klass, obj_id)
      return klass[obj_id] unless Gui.policy_active?
      scope = policy_scope(klass)
      if scope.is_a?(Proc)
        # The proc expects current user and an array of objects (TODO or maybe the proc expects a dataset?)
        scope.call(current_user, [klass[obj_id]]).first
      else
        scope.where(:id => obj_id).first
      end
    end
    
    # # when fetching a collection of objects
    # def collection_policy_scope(klass, filter = nil)
    #   return klass unless Gui.policy_active?
    #   scope = policy_scope(klass)
    #   if scope.is_a?(Proc)
    #     # The proc expects current user and an array of objects
    #     scope.call(current_user, [klass[obj_id]]).first
    #   else
    #     # puts Rainbow('obj_policy_scope').yellow
    #     # puts scope.inspect
    #     scope[obj_id]
    #   end
    # end
  end
end
