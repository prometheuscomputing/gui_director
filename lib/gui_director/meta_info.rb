# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module GuiDirector
  GEM_NAME = "gui_director"
  VERSION  = '7.2.0'
  AUTHORS  = ["Samuel Dana", "Ben Dana", "Michael Faughn"]
  SUMMARY  = %q{Displays Gui's based on a given reference implementation.}
  EMAILS      = ["s.dana@prometheuscomputing.com", "michael.faughn@nist.gov"]
  HOMEPAGE    = 'https://gitlab.com/prometheuscomputing/gui_director'
  DESCRIPTION = %q{AJAX web application for viewing and editing data structured by a UML model.}
  
  LANGUAGE         = :ruby
  LANGUAGE_VERSION = ['>= 2.7']
  RUNTIME_VERSIONS = {
    :mri => ['>= 2.7']
  }
  
  DEPENDENCIES_RUBY = {
    'lodepath'                     => '~> 0.1',
    'common'                       => '~> 1.12',
    'sequel_specific_associations' => '~> 11.0',
    'sequel_change_tracker'        => '~> 2.15',
    'gui_spec'                     => '~> 1.6',
    'pundit'                       => '~> 2.1',
    'concurrent-ruby'              => '~> 1.1',
    'activesupport'                => '~> 7.0',
    'multivocabulary'              => '~> 0.1', # Required if multivocabulary option is turned on
    # Defining SSA dependency since we now directly rely on it for Class #parents and #children relationships
    'titleize'                     => '~> 1.4',
    'blankslate'                   => '~> 3.1',
    'mime-types'                   => '~> 3.3',
    'chronic'                      => '~> 0.10',
    'rainbow'                      => '~> 3.0',
    'pundit'                       => '~> 2.1'
  }
  DEVELOPMENT_DEPENDENCIES_RUBY = {
    'rspec'                 => '~> 3.0',
    'car_example_generated' => '~> 2.7',
    'car_example'           => '~> 1.27'
  }
end
