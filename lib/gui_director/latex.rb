module Gui
  require 'sys/proctable'
  require 'timeout'
  class Latex
    include Sys
    PDFLATEX_TIMEOUT  = 12#0.0
    LATEX_TIMEOUT     = 45.0
    DVIPS_TIMEOUT     = 45.0
    CONVERT_TIMEOUT   = 45.0
    INCREMENT_DEFAULT = 0.1
    @@numbered_latex_code_buffer = ""
    require 'tempfile'
    require 'logger'
    # Do Logging initialization
    object_logger Logger.new(File.join(LOG_DIR, 'latex.log'))
    object_logger.log_warn!
    
    TEX_DIR = File.join(DATA_DIR, 'tex')
    Dir.mkdir(TEX_DIR) unless File.exist?(TEX_DIR)
    
    def self.add_custom_latex_processing name_proc_hash
      if defined?(@custom_latex_processings)
         @custom_latex_processings = @custom_latex_processings.merge(name_proc_hash)
      else
        @custom_latex_processings = name_proc_hash
      end
    end

    def self.clear_custom_latex_processings
      @custom_latex_processings = {}
    end

    def self.default_preamble
      if defined?(@default_preamble)
        @default_preamble
      else
        f = ""
        f << "\\documentclass{article}\n"
        f << "\\usepackage{ulem}\n" # underlining for emphasis
        f << "\\usepackage{fixltx2e}\n" # fixes to latex that can't be included in kernel
        f << "\\usepackage[utf8]{inputenc}\n"
        f << "\\usepackage[T1]{fontenc}\n" # are we sure that this is good? we're pretty sure that it might be bad not to include it...
        f << "\\usepackage{textcomp}\n"
        f << "\\usepackage{amsmath}\n"
        f << "\\usepackage{amssymb}\n"
        f << "\\usepackage{mathtools}\n"
        f << "\\usepackage{xfrac}\n" # provides slanted fractions
        f << "\\usepackage{siunitx}\n"
        f << "\\usepackage{booktabs}\n"
        f << "\\usepackage{float}\n"
        f << "\\usepackage{nopageno}\n"
        f << "\\usepackage[margin=1.0in]{geometry}\n"
        f << "\\restylefloat{figure}\n"
        f << "\\setlength{\\parindent}{1em}    % sets leading space for paragraphs\n"
        f << "\\pagestyle{empty}\n"
        f << "\\usepackage{enumitem}\n"
        f << "\\begin{document}\n"
        f << "\\raggedright\n"
        f
      end
    end

    def self.default_preamble=(preamble_override)
      @default_preamble = preamble_override
    end

    def self.latex_code_to_html_code(latex_code)
      html_code = "<pre style='text-align: left; white-space: pre-line; line-height: 1em;'>"
      html_code << latex_code
      html_code << "</pre>"
      html_code
    end

    def self.render_data_to_html(data, options = {})
      return nil unless data && !data.empty?
      locals = {}
      image = render_data_to_image(data, options)
      raise "Failed to find image file" unless image
      locals[:image_path] = image.path
      Haml::Engine.new(File.read(relative('../../lib/gui_builder/view/image_content.haml'))).render(self, locals)
    end

    # FIXME this should not be here!
    def self.create_image_popup(image_url)
      raise "Please refactor this method so it isn't in gui_director." #remove this if you must....better to actually fix things
      locals = {}
      locals[:image_url] = image_url
      Haml::Engine.new(File.read(relative('../../lib/gui_builder/view/image_content.haml'))).render(self, locals)
    end

    # NOTE: using tempfiles with threads was problematic with Open4 so we use regular files instead and just clean them up manually
    # We have since abandoned Open4 but things are working as is so leave alone...can't use temp files for everything latex produces anyway
    # So must clean up anyway...
    def self.render_data_to_image(data, options = {})
      return nil unless data && !data.empty?
      type = options[:type] || :png
      image_extension =  case type
                                    when :png
                                      '.png'
                                    when :jpg
                                      '.jpg'
                                    end
      tex_filename = create_tex_from_data(data, options)
      tex_filename_base = tex_filename.gsub(/\.tex$/, "")
      this_dir = Dir.pwd
      Dir.chdir(TEX_DIR)
      # imagefile = File.new("image_of_#{tex_filename_base}#{image_extension}", "w+")
      image_filename = "image_of_#{tex_filename_base}#{image_extension}"
      # Note the '-append' option when converting specifies that there should be only one large image file
      # instead of a separate image per page. If we want to implement a 'page-by-page' reader, remove this flag
                        # cd #{TEX_DIR} && dvips -D 1200 -o #{tex_filename_base}.ps #{tex_filename_base}.dvi  &&
      cmd1 = "latex -interaction=nonstopmode #{tex_filename}"
      latex_thread = exec_in_managed_thread cmd1, {  :timeout => LATEX_TIMEOUT, :combine_stderr_stdout => true }

      dvips_thread = nil
      convert_thread = nil
      
      if latex_thread && latex_thread[:success]
        cmd2 = "dvips -D 1200 #{tex_filename_base}.dvi"
        dvips_thread = exec_in_managed_thread cmd2, { :timeout => DVIPS_TIMEOUT, :combine_stderr_stdout => true }
        if dvips_thread && dvips_thread[:success]
          cmd3 = "convert -append -background white -flatten -density 96x96 #{tex_filename_base}.ps #{image_filename}"
          convert_thread = exec_in_managed_thread cmd3, { :timeout => CONVERT_TIMEOUT, :combine_stderr_stdout => true }
        end
      else
        # clean up and throw LatexError
        # is it possible to close the popup if this failed?
      end

      if dvips_thread
        log_data = clean_latex_output(latex_thread[:stdout] + dvips_thread[:stdout])
      else
        log_data = clean_latex_output(latex_thread[:stdout])
      end
      log_data << convert_thread[:stdout]  if convert_thread
      log_info(log_data)

      success = convert_thread && convert_thread[:success] || false
      
      if success
        log_info {"*" * 40 + "\nPDF AND IMAGE CREATION SUCCESSFUL: " + success.to_s.upcase + "\n"  + "*" * 40 }
        image = File.read(image_filename)
        # Clean up latex files.
        remove_latex_files tex_filename_base
        #  Clean up image file
        FileUtils.rm(Dir.glob("#{image_filename}")) rescue nil
        # Go back to where we started
        Dir.chdir(this_dir)
        return image
      else
        log_error(@@numbered_latex_code_buffer)
        log_error(log_data)
        log_error( "Creation of image from pdflatex FAILED !!!") unless success
        # Clean up latex files.
        remove_latex_files tex_filename_base
        #  Clean up image file
        FileUtils.rm(Dir.glob("#{image_filename}")) rescue nil
        # Go back to where we started
        Dir.chdir(this_dir)
        if latex_thread && latex_thread[:success]
          raise ImageError, "Failed to produce image"
        else
          raise LatexError, "latex Failed.  See log files for details."
        end
        return nil
      end
    end

    # NOTE: using tempfiles with threads is problematic so we use regular files instead and just clean them up manually
    def self.render_data_to_pdf(data, options = {})
      return nil unless data && !data.empty?
      tex_filename = create_tex_from_data(data, options)
      tex_filename_base = tex_filename.gsub(/\.tex$/, "")
      # latex_stdin = ''
      # latex_stdout_stderr = ''
      # cmd = "cd #{TEX_DIR} && pdflatex -interaction=nonstopmode #{tex_filename}"
      cmd = "cd #{TEX_DIR} && pdflatex -interaction=nonstopmode #{tex_filename}"
      pdflatex_thread = exec_in_managed_thread cmd, { :timeout => PDFLATEX_TIMEOUT, :combine_stderr_stdout => true }

      cleaned_latex_output = clean_latex_output(pdflatex_thread[:stdout])
      log_info(cleaned_latex_output)

      if pdflatex_thread[:success]
        pdf_result = File.read("#{TEX_DIR}/#{tex_filename_base}.pdf") # What if this gets hosed?
        log_info {"*" * 40 + "\nPDF CREATION SUCCESSFUL: " + pdflatex_thread[:success].to_s + "\n" + "*" * 40}
        # Clean up latex files.
        remove_latex_files tex_filename_base
        pdf_result
      else
        log_error(@@numbered_latex_code_buffer)
        log_error(cleaned_latex_output)
        # Clean up latex files.
        remove_latex_files tex_filename_base
        raise LatexError, "pdflatex failed.  See log file for details."
      end
    end

    def self.remove_latex_files file_name_base
      this_dir = Dir.pwd
      Dir.chdir(TEX_DIR)
      puts "Remove latex files from: #{file_name_base}" if $verbose
      FileUtils.rm(Dir.glob("#{file_name_base}.aux")) rescue nil
      FileUtils.rm(Dir.glob("#{file_name_base}.log")) rescue nil
      FileUtils.rm(Dir.glob("#{file_name_base}.tex")) rescue nil
      FileUtils.rm(Dir.glob("#{file_name_base}.pdf")) rescue nil
      FileUtils.rm(Dir.glob("#{file_name_base}.ps"))  rescue nil
      FileUtils.rm(Dir.glob("#{file_name_base}.dvi")) rescue nil
      Dir.chdir(this_dir)
    end

    # Just gets rid of a bunch of noise from latex output
    def self.clean_latex_output input
       begin
        cleaned_latex_output = input
        cleaned_latex_output.gsub!(/.*r-basic-dictionary\/translator-basic-dictionary-English.dict/m, '')
        cleaned_latex_output.gsub!(/(Transcript|Output) written on guibuilder_latex*.*/m, '')
      rescue StandardError => e
        cleaned_latex_output = input
        puts "ERROR CLEANING LATEX CONSOLE OUTPUT"
        sleep(1)
        puts e.to_s
        # puts cleaned_latex_output.inspect
        puts
      end
      cleaned_latex_output
    end

    def self.exec_in_managed_thread(cmd, options = {})
      success  = false
      finished = false
      message   = options[:message]   || "Executing thread:"
      maxt      = options[:timeout]   || TIMEOUT_DEFAULT
      increment = options[:increment] || INCREMENT_DEFAULT
      # stdin = ""
      stdout = ""
      stderr = ""
      stderr = stdout if options[:combine_stderr_stdout]
      
      puts message + " " + cmd if $verbose
      begin
        pipe = IO.popen(cmd, 'r')
        pid = pipe.pid
        puts "Started process #{pid}" if $verbose
      rescue StandardError => e
        puts e.message
        puts e.backtrace
        stderr << 'Execution of ' + cmd + " has failed :\n    " + e.to_s
        # raise "run failed"
      end
      
      begin
        puts "Waiting for pipe to return" if $verbose
        Timeout::timeout(maxt) do |t|
          a = Process.waitpid2(pid)
          stdout << pipe.gets(nil).to_s
          finished = true
          if a[1].exitstatus != 0
            puts "FAILURE OF: #{cmd}"
            stderr << 'Error while executing: ' + cmd
            # raise "run failed"
          else
            success = true
          end
        end
      rescue Timeout::Error => e
        puts "Timeout::Error for #{cmd}"
        stderr << 'Execution of ' + cmd + ' has timed out. Killing subprocess(es).'
        begin
          all_processes = ProcTable.ps
          bad_processes = [ProcTable.ps(pid)]
          cmd.split(/&&|;/).each { |command| bad_processes << all_processes.reject { |prcs| prcs.cmdline != command.strip } }
          # bad_processes.each { |p| Process.kill('KILL', p.pid) }
          bad_processes.flatten.each { |prcs| puts prcs.pid; Process.kill('KILL', p.pid) }
          pipe.close
        rescue Object => e
          puts 'Failed killing process : ' + e.to_s
          stderr << 'Failed killing process : ' + e.to_s
        end
        # raise "run failed"
      end
      # stdout = pipe.gets(nil)
      pipe.close unless pipe.closed?
      puts stderr
      puts "Managed thread finished." if $verbose
      {:finished => finished, :success => success, :stdout => stdout, :stderr => stderr}
    end

    def self.create_tex_from_data(data, options = {})
      defaults = {:add_preamble => true}
      options = defaults.merge(options)

      filepath = nil
      tempfile = nil
      tempdir  = nil
      processed_data = process_before_latex(data)
      now = Time.now.strftime('%Y%m%d_%H%M%S')
      tex_filename = "guibuilder_latex_#{now}.tex"
      this_dir = Dir.pwd
      Dir.chdir(TEX_DIR)
      f= File.open(tex_filename, "w+")
      f.puts self.default_preamble if options[:add_preamble]
      f.puts processed_data.to_s
      f.puts '\end{document}'

      f.rewind
      lines = f.readlines
      numbered_lines = ""
      lines.each_with_index { |l, i| numbered_lines << sprintf("%4i: %s", i+1, l) }
      f.rewind
      log_info "\n\n#{numbered_lines}\n\n"
      @@numbered_latex_code_buffer.clear
      @@numbered_latex_code_buffer << "\n\n#{numbered_lines}\n\n"
      f.close
      Dir.chdir(this_dir)
      filepath = f.path
      return filepath
    end

    def self.process_before_latex data
      output = String.new(data)
      output = do_custom_latex_processings output if defined?(@custom_latex_processings)
      output = handle_special_latex_chars(output)
      output
    end

    def self.do_custom_latex_processings input
      output = String.new(input)
      output = remove_nested_math_mode output
      @custom_latex_processings.each do |name, proc|
        log_info "Doing custom LaTeX processing for #{name}"
        output = proc.call(output)
      end
      output
    end

    def self.remove_nested_math_mode input
      # output = String.new(input)
      output = ""
      braces = 0
      math_mode_on = false
      input.each_char do |c|
        case
        when c == '$'
          if braces < 1
            output << c
            math_mode_on = !math_mode_on
          end
        when c == '{' && math_mode_on
          braces += 1
          output << c
        when c == '}'  && math_mode_on
          braces -= 1
          output << c
        else
          output << c
        end
      end
      output
    end

    def self.escape_ampersands_except_in_tables input
      lines = input.split("\n")
      output = ""
      table_on = false
      lines.each do |line|
        table_on = true if line =~ /begin{table/
        table_on = false if line =~ /end{table/
        if table_on
          output << line + "\n"
        else
          editted = line.gsub(/(?<!\\)&/) {'\&'}
          output << editted + "\n"
        end
      end
      output
    end


    # this may not be comprehensive..additional special chars may have special meaning in the old formatting
    #  regime and will need to be handled.  NOTE: These could be a real problem if / when people start using these symbols in the editor as they are
    #  intended to be used by LaTeX.  By prepending them with backslash we are making it impossible for them to use the symbols as markup language.
    def self.handle_special_latex_chars(input)
      log_info "Handling special latex chars"
      output = input.gsub(/~( ?\d+(?:\.\d+)?)/,'\\num{\sim \1}') # this may have problems.  check tildas in text TODO
      output = escape_ampersands_except_in_tables(output)
      output.gsub!(/#/) {'\#'}
      output.gsub!(/_(?=[^{])/) {'\_'}
      output.gsub!(/\. (?=\S)/,".~") # Make sure there is only one space after an abbrev. that ends with a period (i.e. not two spaces for a new sentence)
      output
    end

  end
end
