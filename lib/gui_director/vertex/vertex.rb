require 'securerandom'
require 'chronic'

module Gui
  class Vertex
    include Gui::PolicyMethods
    attr_reader :current_user
    attr_reader :widget
    # TODO domain_obj is a bad name for what this is.  domain_obj implies that it is an object from the application domain but in reality this could be some other type of object, e.g. an AdHocPageObject
    attr_reader :domain_obj
    attr_reader :parent
    attr_reader :children
    attr_reader :gui_root
    attr_reader :pending_association_additions
    attr_reader :pending_association_removals
    attr_reader :pending_association_deletions
    attr_reader :previous_object_state
    attr_reader :initial_object_state
    attr_reader :multiple
    # A list of additional classes to render for this widget/domain_obj
    attr_reader :classes
  
    # root_opts are only passed in while creating a root vertex
    def initialize(user:nil, widget:, parent_vertex:nil, obj:nil, obj_state:{}, root_opts:{})
      @current_user = user || parent_vertex&.current_user
      raise "Vertex must be associated with a user!" unless @current_user
      @widget       = widget
      @filter       = []
      @children     = []
      @parent       = parent_vertex
      @parent.children << self if @parent
      @gui_root     = root_opts.any?
    
      case obj
      when ORM_Class, Gui::AdHocPageObject
        @domain_obj = obj
        @domain_classifier = @domain_obj.is_a?(Hash) ? @domain_obj[:to].class : @domain_obj.class
      when NilClass
        @domain_obj = nil
      else
        raise "This object is not suitable for a Vertex! #{obj.inspect}\n#{obj.is_a?(Sequel::Model)}\n#{obj.class.ancestors.inspect}"
      end
        
      @domain_obj_is_nil = false # What? Even if it is in fact nil?  WTF???? FIXME
    
      get_parent_obj

      # Get assoc or attribute info
      # Note that this relies on checking whether the parent vertex has a cached domain object in order to
      #   determine whether or not it is a valid vertex to check for association/attribute information on.
      # Currently, this isn't a problem since Organizer widgets always have a cached domain object, but it should
      #   be replaced eventually. -SD
      # TODO: should not skip if parent is multiple, since we still want attribute information
      if has_cached_parent_obj? && getter && !@parent.multiple && @parent_obj
        info = @parent_obj.class.properties[getter.to_sym] || {}
        if info
          @assoc_info = info if info[:association]
        else
          puts "#{@parent_obj.class} had no info for #{getter}"
        end
      end

      @is_association = !@assoc_info.nil?
    
      # Whether or not the vertex is derived. Alias associations, although derived, are not considered derived vertices.
      @is_derived = @is_association && (@assoc_info[:derived] && !@assoc_info[:alias])
      @is_primitive_association = @is_association && @assoc_info[:primitive]
      @multiple = @assoc_info[:type].to_s =~ /to_many$/ if @is_association
      @classes  = widget ? widget.classes : []
      @classes  = root_opts[:classes] || @classes # I have no idea why we are doing this...

      setup_pending_associations(obj_state, root_opts)

      build_children(obj_state, widget) if (widget.is_a?(Gui::Widgets::ViewRef) || widget.is_a?(Gui::Widgets::View))
    
      # TODO: this shouldn't go here.  Why not?  Where should it go?
      Gui.use_options([:collection_content, :filter, :limit, :offset, :order, :search_all_filter])
      if root_opts[:collection_content]
        where = root_opts[:filter] || {}
        where.merge!(widget.filter_value) if widget.filter_value
        add_to_filter(where)
        @limit  = root_opts[:limit]
        @offset = root_opts[:offset]
        @order  = root_opts[:order]
        @search_all_filter = root_opts[:search_all_filter]
      end
    end

    # For viewrefs and views that need to have content loaded, recursively add child vertices.
    def build_children(obj_state, parent_widget)
      lazy = parent_widget.lazy_load_content
      widget.content.each do |w|
        next if lazy # leaving this here in case there is ever a situation where we want to force loading of content even if parent is lazy_load.  e.g. next if lazy && !w.unlazy
        if w.getter.nil? || obj_state&.[]('content').nil?
          child_obj_state = {}
        else
          child_obj_state = obj_state['content'].find { |child_obj_hash| child_obj_hash['getter'] == w.getter.to_s } || {}
        end
        Vertex.new(widget:w, parent_vertex:self, obj_state:child_obj_state)
      end
    end
  
    def parent=(parent_val)
      if parent_val
        @parent = parent_val
        @parent.children << self
      end
    end
  
    # Uniquely identifies this vertex on a page
    def pretty_id
      @pretty_id ||= "#{widget.class.to_s.split('::').last}_#{id}"
    end
  
    # Returns an array of vertex pretty_id's that terminate at this vertex
    def pretty_path
      path = [pretty_id]
      path = @parent.pretty_path + path if @parent
      path
    end
  
    # Identifies the piece of data used by the widget on a page
    # It is possible that multiple widgets are backed by the same piece of data, so this is not unique on a page.
    def pretty_name
      @pretty_name ||= "#{@parent.domain_obj.class.to_s.gsub(':', '-')}__#{@parent.domain_obj.id}__#{getter}"
    end
  
    def id
      @id ||= VertexCounter.increment.to_s
    end
  
    def uuid
      SecureRandom.uuid
    end

    def parent_domain_obj
      return nil unless @parent
      @parent.domain_obj
    end
  
    def parent_through_obj
      return nil unless @parent
      @parent.through_obj
    end
  
    # Get the parent_domain_obj or the parent_through_obj depending on the widget
    def get_parent_obj
      @parent_obj ||= (widget && widget.association_class_widget) ? parent_through_obj : parent_domain_obj
    end
      
    def has_cached_parent_obj?
      return false unless @parent
      @parent.has_cached_domain_obj?
    end

    def has_cached_domain_obj?
      case @domain_obj
      when NilClass
        false
      when Hash
        !@domain_obj[:to].nil?
      else
        true
      end
    end

    # Given a Class, returns an array of ancestor Classes (but no module mixins like #ancestors) S.D.
    def parents(obj)
      (obj.superclass ? parents(obj.superclass) : []) << obj
    end

    # TODO: Fix weird use of #eql? vs #== (#eql? is checking parents, #== is not) S.D.
    # TODO: recursive check bad for efficiency.
    def eql?(other)
      other.kind_of?(Gui::Controller::Vertex) && self.widget.eql?(other.widget) && @parent.eql?(other.parent)
    end

    def ==(other)
      other.kind_of?(Gui::Controller::Vertex) && other.domain_obj == self.domain_obj &&
        other.widget == self.widget
    end
  
    # Should probably normally use the cached version, #domain_obj.
    # Regarding policy -- We have already determined that @parent_obj is authorized.  We only need be concerned with authorizing associated objects and attribute values
    def get(get_associations_hash = false, get_unassociated_objects = false)
      raise "Cannot call get for a label widget!" if widget.is_a?(Gui::Widgets::TextLabel)
      raise "Cannot call get for a link widget!" if widget.is_a?(Gui::Widgets::Link)
    
      # TODO: remove widget-specific code
      return @parent_obj if widget.is_a?(Gui::Widgets::Context) && (!getter || getter.empty?) # FIXME this should not be possible, need to raise if this happens
      return nil if widget.is_a?(Gui::Widgets::HTML) && (!getter || getter.empty?)
    
      # Return a new instance of the data_classifier unless there is a parent_obj
      # TODO Is there a case in which we could have gotten here where creation of a parent_obj would not be authorized?  What happens to this newly created parent object?
      unless @parent_obj
        puts Rainbow("About to create @parent_obj for #{data_classifier.concrete_class}.#{getter}").orange
        cc = widget.data_classifier.concrete_class
        authorize_creation(cc)
        return widget.data_classifier.concrete_class.new
      end
      
      getter_method = getter.to_s
      getter_method += '_unassociated' if get_unassociated_objects
      
      if @is_primitive_association
        primitive_values = get_authorized_property(@parent_obj, getter_method)
        # puts Rainbow(data_classifier.inspect).green
        # FIXME this should not depend on checking a string when we have a klass to work with...
        case data_classifier.name
        when 'Integer', 'Float'
          return primitive_values.map(&:to_s).join(' ')
        when 'String'
          return primitive_values.map(&:inspect).join(' ')
        else
          raise "#{data_classifier} is not a supported primitive for attributes with a multiplicity greater than one."
        end
      end
      
      return get_associated(getter_method, get_associations_hash, get_unassociated_objects) if @is_association

      @domain_obj = get_authorized_property(@parent_obj, getter_method)
      @domain_obj_is_nil = @domain_obj.nil?
      @domain_obj
    end
    
    def get_associated(getter_method, get_associations_hash, get_unassociated_objects)
      puts Rainbow("INFO (@parent_obj is nil):").orange unless @parent_obj
      puts info unless @parent_obj
        
      # Get an array of filters from the "filter" variable
      query = (filter && !filter.empty?) ? Gui.build_query_from_filter(filter) : {}

      # This will work with 'search all' query and advanced search filters at the same time although this will probably be disabled in the Gui by not allowing both searches
      search_all_query = Gui.build_query_from_search_all_filter(search_all_filter)
      if query && !query.empty?
        query = !search_all_query.empty? ? query + search_all_query : query
      else
        query = !search_all_query.empty? ? search_all_query : query
      end
  
      if @multiple || get_unassociated_objects
        args = {filter:query, limit:limit, offset:offset, order:order}
        args[:return_associations_hash] = true unless get_unassociated_objects
        objects = get_authorized_property(@parent_obj, getter_method, args)
        @domain_obj ||= []
        # raise "Pagination was not applied correctly. Limit: #{limit} Return: #{ret.size}" if limit && limit > 0 && ret.size > limit
        # TODO this is just nuts...why are we doing this?
        @domain_obj[offset..(offset + objects.size - 1)] = objects if objects.any?
        # @domain_obj_is_nil # FIXME is this just not important in this case?  It was never being set...
        return objects
      end
      
      # if widget.is_a?(Gui::Widgets::Context) && widget.auto_create
      if widget.context?
        auto_create_classes = widget.is_a?(Gui::Widgets::ViewRef) ? Array(widget.view.auto_create) : Array(widget.auto_create)
        object = @parent_obj
        unless object.nil?
          assoc_info = object.class.associations[getter.to_sym]
          raise "Bad getter: #{getter_method} called on #{object.class}" unless assoc_info
          # FIXME policy here
          object = get_authorized_property(@parent_obj, getter_method)
          unless object
            auto_create_class = auto_create_classes.shift
            auto_create_class = assoc_info[:class].to_const.concrete_class unless auto_create_class.is_a?(Class)
            authorize_creation(auto_create_class)
            object = auto_create_class.new
            # Well, this is the band-aid that Sam and I talked about on Aug 23, 2016.
            object.id = "new_#{getter}#{get_usable_parent_id_string}" # NOTE: this may not work in all cases

            #if there is a through and get_associations_hash is true then return the through and to hash
            if get_associations_hash && assoc_info[:through]
              through_class  = assoc_info[:through].to_const
              authorize_creation(through_class)
              through_obj    = through_class.new
              through_obj.id = "new_#{g}_through#{get_usable_parent_id_string}"
              object = {:to => object, :through => through_obj}
            end
          end
        end
        @domain_obj_is_nil = object.nil?
        @domain_obj = object
        @domain_obj
      end

      # to-one, not a context
      if get_associations_hash
        # policy here
        object = get_authorized_property(@parent_obj, getter_method, {return_associations_hash: :both})
      else
        object = get_authorized_property(@parent_obj, getter_method)
      end
      @domain_obj_is_nil = object.nil?
      @domain_obj = object
      return @domain_obj
    end
  
    def get_usable_parent_id_string
      if @parent
        pdo = parent_domain_obj
        if pdo && pdo.is_a?(ORM_Class)
          if pdo.id
            "_for_#{pdo.class}_#{pdo.id}"
          else
            "_for_#{pdo.class}#{parent.get_usable_parent_id_string}"
          end
        else
          @parent.get_usable_parent_id_string
        end
      end
    end 
    
    # Get the number of associated objects that match the applied filter.
    def get_filtered_count(get_unassociated_objects = false)
      return @filtered_count if @filtered_count
      raise "Cannot call get for a label widget!" if widget.is_a?(Gui::Widgets::TextLabel)
      raise "Cannot call get for a link widget!" if widget.is_a?(Gui::Widgets::Link)
      # Don't return 0 when looking for unassociated organizers
      return 0 if widget.view && widget.view.is_a?(Gui::Widgets::Organizer) && !get_unassociated_objects

      getter_method = getter.to_s
      getter_method += '_unassociated' if get_unassociated_objects

      # Return a zero unless there is a parent_obj
      return 0 unless @parent_obj
    
      # Get an array of filters from the "filter" variable
      query = (filter && !filter.empty?) ? Gui.build_query_from_filter(filter) : {}
    
      # This will work with Search all query and normal search filters at the same time although this will probably be disabled in the Gui by not allowing both searches
      search_all_query = (search_all_filter && !search_all_filter.empty?) ? Gui.build_query_from_search_all_filter(search_all_filter) : []
      # puts search_all_query.inspect
      if (query && !query.empty?)
        query = !search_all_query.empty? ? query + search_all_query : query
      else
        query = !search_all_query.empty? ? search_all_query : query
      end
        
      # Call the getter
      if @is_association && (@multiple || get_unassociated_objects)
        # FIXME policy here
        ret = @parent_obj.send((getter_method + '_count').to_sym, query)
      elsif @is_association
        ret = 0
      else
        raise 'Cannot call on object that is not an association'
      end
      @filtered_count = ret
    end

    # Just call #get with a few minor changes
    def get_associations
      get(true)
    end
  
    # Get the total number of associated objects.
    # This won't be valid for all domain objects, only associations
    def get_count
      # Using the @domain_obj.count as is makes us susceptible to pagination, but
      # works for now as we only load the count once at page load.
      # return @domain_obj.count if defined?(@domain_obj) && @domain_obj.is_a?(Array)
      raise "no widget defined for this Vertex" unless widget
      return 0 if widget.view && widget.view.is_a?(Gui::Widgets::Organizer)
      return 0 unless @parent_obj
      # Do not attempt to get counts for parent_objs with placeholder ids (like 'new_1')
      # This is used by gui_site's #render_document_association_item
      # Intead, return a count of 1, as this is the only case for the current usage.
      # FIXME review this for usage with Context widgets
      return 1 if @parent_obj.id.is_a?(String)
    
      accessor = getter.to_s
      count_accessor = accessor + '_count'
    
      query = filter ? Gui.build_query_from_filter(filter) : []
    
      # Fall back to inefficiently counting domain_obj if association is derived and does not
      # implement count accessor
      if @is_derived && !@parent_obj.respond_to?(count_accessor)
        case domain_obj
        when Array
          return domain_obj.count
        when NilClass
          return 0
        else
          return 1
        end
      end
      # FIXME policy scope here (count)
      ret = @parent_obj.send(count_accessor, query)
      @domain_obj_count = ret
      ret
    end

    def get_unassociated_count
      return 0 if widget.view && widget.view.is_a?(Gui::Widgets::Organizer)
      accessor = getter.to_s
      count_accessor = accessor + '_unassociated_count'
      return 0 unless @parent_obj

      raise "no widget defined for this Vertex" unless widget
      # FIXME policy scope here (count)
      ret = @parent_obj.send(count_accessor)
      @domain_obj_unassociated_count = ret
      ret
    end
  
    # Return the (potentially cached) domain_obj, according to specified pagination settings for vertex
    # If get_associations_hash is false, @domain_obj will be stripped of its association hash if present
    # This method sucks. It does too much crap and it is used in too many different ways by clearview and advanced view by too many different types of objects and datatypes. -TB
    def domain_obj(get_associations_hash = false)
      return nil if widget && widget.data_classifier == NilClass
      if @domain_obj
        if multiple
          end_offset = limit && limit > 0 ? offset + limit - 1 : -1
          result = @domain_obj[offset..end_offset]
        # TODO This case shouldn't even exist.  I don't think domain_obj should be used for through_obj at all.  This really needs to be refactored into multiple methods -TB
        elsif get_associations_hash && !@domain_obj.is_a?(Hash) && getter
          result = get_associations
        else
          result = @domain_obj
        end
      else
        if @domain_obj_is_nil
          result = nil
        else
          result = get(get_associations_hash)
        end
      end

      if !@is_derived && !get_associations_hash && !@domain_obj_is_nil && !@is_primitive_association
        if multiple
          result = result.map { |obj| obj[:to] if obj.is_a?(Hash) }.compact
        else
          result = result[:to] if result.is_a?(Hash)
        end
      end
      result
    end
  
    def through_obj
      obj_hash = domain_obj(true)
      return nil unless obj_hash
      return nil if obj_hash.is_a?(Array) && obj_hash.empty?
      if multiple
        obj_hash.map { |obj| obj[:through] }
      else
        obj_hash[:through]
      end
    end
  
    def domain_classifier
      if @domain_obj && domain_obj.is_a?(ORM_Class)
        domain_obj.class
      else
        @domain_classifier || widget.data_classifier
      end
    end
    def through_classifier
      if through_obj && through_obj.is_a?(ORM_Class)
        through_obj.class
      else
        @through_classifier || widget.data_classifier
      end
    end
  
    def parent_domain_classifier
      @parent.domain_classifier if @parent
    end
    def parent_through_classifier
      @parent.through_classifier if @parent
    end
    def parent_classifier
      (widget && widget.association_class_widget) ? parent_through_classifier : parent_domain_classifier
    end
  
    def domain_id
      (domain_obj && domain_obj.is_a?(ORM_Class)) ? (domain_obj.id || 'new') : nil
    end
    def through_id
      (through_obj && through_obj.is_a?(ORM_Class)) ? (through_obj.id || 'new') : nil
    end
  
    def parent_domain_id
      @parent.domain_id if @parent
    end
    def parent_through_id
      @parent.through_id if parent
    end
    def parent_id
      (widget && widget.association_class_widget) ? parent_through_id : parent_domain_id
    end

    def domain_obj_associations
      domain_obj(true)
    end



    def add_to_filter(where)
      # this should ensure that we don't repeatedly add the same filter
      @filter << where unless @filter.include?(where)
    end
  
    def filter
      if widget.is_a?(Gui::Widgets::ViewRef) && widget.view
        fv = widget.view.filter_value
        add_to_filter(fv) if fv
      end
      @filter
    end

    def limit
      # TODO: track down where limit is not being set from
      @limit
    end

    def offset
      @offset ||= 0
    end
  
    def order
      @order ||= {}
    end
  
    def search_all_filter
      @search_all_filter ||= {}
    end

    def domain_obj_count
      @domain_obj_count ||= get_count
    end

    def domain_obj_unassociated_count
      @domain_obj_unassociated_count ||= get_unassociated_count
    end

    # Always an array of unassociated objects
    def unassociated_obj
      if @unassociated_obj
        end_offset = limit && limit > 0 ? offset + limit - 1 : -1
        result = @unassociated_obj[offset..end_offset]
        result
      else
        @unassociated_obj = get_unassociated
      end
    end

    def get_unassociated
      get(false, true)
    end

    def save
      @domain_obj.save
    end
  
  end
end
