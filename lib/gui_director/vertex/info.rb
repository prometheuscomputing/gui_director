module Gui
  class Vertex
    def to_s
      label = "#{widget.data_classifier}"
      label += " (#{widget.getter})"
    end
    
    def info(indent = 0)
      out = ''
      w = widget
      out << (" " * indent)
      out << w.class.to_s
      if (w.getter && !w.getter.empty?) || parent.nil?
        str = w.getter ? ":#{w.getter}" : "ROOT_OBJ"
        out << "(#{str} -(type)-> "
        if w.is_a?(Gui::Widgets::ViewRef)
          out <<"#{w.view.class.inspect} for #{w.view.data_classifier.to_s}"
        else
          out << w.data_classifier.to_s
        end
        out << (multiple ? "(0..*)" : "(0..1)") if w.getter
        out << ")"
      end
      # out = Rainbow(out).green
      case widget
      when Gui::Widgets::TextLabel
        # this assumes that the builder is for HTML, which it may not be!
        out << " #{widget.text.to_s[0..50].inspect}"
      else
        out << " #{send(:instance_variable_get, :@domain_obj).inspect}"
      end
      out
    end
    
    def info_tree(indent = 0)
      out = ''
      out << Rainbow("\nVertex tree:\n").magenta if indent == 0
      out << "#{info(indent)}\n"
      children.each do |vc|
        out << vc.info_tree(indent + 2)
      end
      out
    end
    
  end
end
