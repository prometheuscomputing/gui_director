require_relative 'vertex'

module Gui
  class Vertex
    # If the parent exists and in the gui, temporarily inherit it's mutablility, else assume
    # we're mutable.  A vertex is immutable if it's widget is disabled or if any of its ancestor verticies are recursively disabled
    # This is extended to cover the more granular functionality in the disabling of widgets.
    # NOTE: There used to be an attempt at caching of whether or not a vertex was disabled for a particular operation but that was broken so it has been removed.  TODO: reimplement some caching that actually works.
    def immutable?(*values)
      return true if widget.disabled?(*values)
      obj = @parent_obj
      opts = {:obj => obj}
      ([:disable] | values).each do |value|
        return true unless true_unless_test_is_true(value, opts)
      end
      return true if parent && !@gui_root && parent.immutable?(:recursive)
      if widget.is_a?(Gui::Widgets::ViewRef)
        wv = widget.view
        return true if wv.disabled?(*values)
        opts = {:obj => obj, :widget => wv}
        ([:disable] | values).each do |value|
          return true unless true_unless_test_is_true(value, opts)
        end
      end
      # Return false as default
      false
    end
    
    def navigable?
      @navigable ||= !immutable?(:disable_traversal)
    end

    def get_tests(operation, opts = {})
      w = opts[:widget] || widget
      w.get_tests(operation)
    end
    
    def true_if_test_is_true(operation, opts = {})
      _test?(operation, false, opts)
    end
    
    def true_unless_test_is_true(operation, opts = {})
      !_test?(operation, false, opts)
    end
    
    def _test?(operation, default_answer, opts = {})
      obj    = opts[:obj] || @parent_obj #|| domain_obj
      # puts Rainbow(obj.inspect).red if opts[:debug]
      tests  = get_tests(operation, opts)
      return default_answer unless tests
      answer = default_answer
      tests.each_with_index do |test, index|
        if test.is_a?(Symbol) && obj.respond_to?(test)
          mthd = obj.method(test)
          return default_answer unless mthd # judgement call here.  ignoring instead of raising.
          case mthd.arity
          when 0
            answer = obj.send(test)
          else
            raise "Widget behavior can be based on methods defined on the model but those methods may not take any arguments.  If you need to control behavior based on both the domain object and the widget then use a proc that accepts both (i.e. proc { |dom_obj, vertex| ... })."
          end
        elsif obj && test.is_a?(Proc)
          w = opts[:widget] || widget
          answer = test.call(obj, w)
        end
        return answer if !!answer != !!default_answer
      end
      default_answer
    end
        
  end
end
