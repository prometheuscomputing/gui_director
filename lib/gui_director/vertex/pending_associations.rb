module Gui
  class Vertex
    private
    def setup_pending_associations(obj_state, opts)
      @pending_association_additions = obj_state['pending_association_additions'] || []
      @pending_association_removals  = obj_state['pending_association_removals']  || []
      @pending_association_deletions = obj_state['pending_association_deletions'] || []
      @previous_object_state         = obj_state['data']
      @initial_object_state          = obj_state['initial_data']
      
      if @is_association && @parent&.domain_obj.is_a?(ORM_Class)
        # We want to know if one is ever going to stomp on the other
        raise if opts[:pending_association_additions] && obj_state['pending_association_additions']
        raise if opts[:pending_association_removals]  && obj_state['pending_association_removals']
        raise if opts[:pending_association_deletions] && obj_state['pending_association_deletions']

        @pending_association_additions = opts[:pending_association_additions] if opts[:pending_association_additions]
        @pending_association_removals  = opts[:pending_association_removals]  if opts[:pending_association_removals]
        @pending_association_deletions = opts[:pending_association_deletions] if opts[:pending_association_deletions]
        
        execute_pending_action(@pending_association_additions, :add)
        execute_pending_action(@pending_association_removals, :remove)
        setup_prepopulated_objects
      end
    end
    
    def setup_prepopulated_objects
      # TODO: should really not get parent_obj here. Could be a collection! -SD
      return unless self.getter && @parent_obj&.respond_to?(:pending_association_additions)

      @parent_obj.pending_association_additions(self.getter.to_sym).each do |addition|
        # Pre-populated objects have already been added to the parent_object, but are needed display purposes only.
        object = {'skip_association' => 'true'}
        object['classifier'] = addition[:object].class.to_s
        object['identifier'] = addition[:object].id.to_s
        if addition[:through]
          object['through_classifier'] = addition[:through].class.to_s
          object['through_identifier'] = addition[:through].id.to_s
        end
        @pending_association_additions << object
      end
    end
  
    # If ID is nill, that means that the object hasn't been created yet. Could modify SSA to display a temporary object on flag passed in from here and pass in the params.
    def execute_pending_action(pending_assocs, action)
      return unless self.getter
      pending_assocs.each do |pending_assoc|
        # TODO: secure this against access to unauthorized objects
        klass = pending_assoc['classifier'].to_const
        id    = pending_assoc['identifier'].to_i if pending_assoc['identifier'] && !pending_assoc['identifier'].empty?
        # Not removing object unless it has an id. If the object doesn't have an ID, then
        # it wouldn't be able to be removed correctly.
        object = klass[id] if id
        # Policy here.  This is happening while building the vertex tree for the next page load.  Maybe we should be taking care of this stuff before building a vertex tree?
        if object
          authorize_association(parent_domain_obj, object)
          parent_domain_obj.send(self.getter.to_s + '_' + action.to_s, object)
        end
      end
    end
  end
end