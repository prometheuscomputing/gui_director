# A thread-safe counter

#  Leaving this here for now just in case AtomicFixnum turns out not to work (cuke test don't explicitly cover thread-safety)
# require 'common/counter' # counter has been removed from common (along with it's dependencies).  If you need it back, use old version of common or put counter and dependencies back into common
# class VertexCounter < Counter_AbstractSingleton
# end
require 'concurrent/atomic/atomic_fixnum'
VertexCounter = Concurrent::AtomicFixnum.new(1)
