module Gui
  class Vertex
    def getter
      @_getter ||= widget&.getter
    end
    
    def view
      @_view ||= widget.is_a?(Gui::Widgets::ViewRef) ? widget.view : widget
    end
    
    def hidden?
      @_hidden ||= widget&.hidden? || true_if_test_is_true(:hide)
    end
    
    # Normally, everything except view_refs are expanded by default.
    # TODO this isn't used by anything at this point
    def expanded?
      @_expanded ||= widget&.expanded || false_unless_test_is_true(:expand)
    end
        
    def is_view?
      @_is_view ||= widget.is_a?(Gui::Widgets::View)
    end

    # a parent can be nil if our widget is a ViewRef, otherwise the parent
    # needs to be set with a valid domain object, or else all getters and
    # setters for the widget will be called on 'nil'
    def renderable?
      return @_renderable if @_renderable
      is_view = widget.kind_of?(Gui::Widgets::ViewRef) || widget.kind_of?(Gui::Widgets::View)
      @_renderable = is_view || @parent_obj
    end

    def data_classifier
      @_data_classifier ||= widget&.data_classifier
    end
    
    def show_multiple_types
      @_show_multiple_types ||= widget&.view.hide_types == false
    end
      
  end
end
