class Regexp
  def self.from_string(regexp_str)
    # If string doesn't start with '/', prepend and append slashes
    regexp_str = "/#{regexp_str}/" unless regexp_str[0] == '/'
    match_data = regexp_str.match(/\/(.*)\/([imxo]*)/)
    unless match_data
      raise ArgumentError, "Could not parse regexp string: #{regexp_str}"
    else
      regexp_content, regexp_options = match_data[1..2]
      regexp_option_consts = []
      regexp_options.split('').each do|o|
        case o
        when /i/i
          regexp_option_consts << Regexp::IGNORECASE
        when /m/i
          regexp_option_consts << Regexp::MULTILINE
        when /x/i
          regexp_option_consts << Regexp::EXTENDED
        when /o/i
          # Do nothing since there is no Regexp const for this option
        else
          # Could raise here due to bad options, but being permissive for now
        end
      end
      begin
        regexp = Regexp.new(regexp_content, regexp_option_consts.inject(:|))
      rescue => e
        raise ArgumentError, "Could not parse regexp string: #{regexp_str}. Received error: #{e.message}"
      end
    end
  end
  
  def to_string
    self.inspect
  end
end