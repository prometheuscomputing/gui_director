class Class  
  def to_title
    # String#to_title is defined in SSA
    self.name.split('::').last.to_title
  end
end