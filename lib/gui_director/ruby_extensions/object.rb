# requiring common/ruby_extensions apparently provides Object#send_chained, Object#respond_to_chained?, and Module#attr_alias -SD
# require 'common/ruby_extensions'

#  NOTE
#  This all looks weird because we're in the process of removing gb_send_complex.  It was called in many contexts and we want to segregate those contexts so we can keep track of what is going on.  To that end, we have several methods that do the same thing.  The different names correspond to where gb_send_complex used to be used in these different contexts.
class Object
  
  # gui_director
  def gb_send_for_vertex_get(meth, *params)
    send(meth, *params)
  end
  
  # gui_director
  def gb_send_for_vertex_get_count(meth, *params)
    send(meth, *params)
  end
  
  # gui_director
  def gb_send_for_pending_assocs(meth, *params)
    send(meth, *params)
  end
  
  # gui_site
  # def gb_send_for_button_action(meth, *params)
  #   send(meth, *params)
  # end
  
  # gui_site
  def gb_send_for_action_download(meth, *params)
    send(meth, *params)
  end

end