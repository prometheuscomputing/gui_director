require 'active_support/inflector'
require 'titleize'
require_relative 'regexp'
class String
  # There is already a #to_regexp, but it doesn't allow definition of options
  def to_full_regexp
    Regexp.from_string(self)
  end
  
  def to_capitalized_title
    to_title.titleize
  end
  
  # Converts snakecase to camelcase.
  # NOTE: converting to snakecase and back to camelcase will not always preserve capitalization
  def to_camelcase
    self.split(/_(?!_)/).map { |s| s[0].upcase + s[1..-1] }.join
  end

  
  # Converts camelcase to snakecase
  # NOTE: uses regexp lookahead to avoid matching multiple capital letters in a row
  #       So 'QWERTYKeyboard'.to_snakecase => 'qwerty_keyboard' and not 'q_w_e_r_t_y_keyboard'
  # If whitespace is found, it is also replaced with an underscore (multiple underscores are merged)
  # 'Car Example'.to_snakecase => 'car_example'
  # 'Car   example'.to_snakecase => 'car_example'
  # 'CarEXAMPLE'.to_snakecase => 'car_example'
  def to_snakecase
    # Insert underscore before capitalized letters that are followed by an uncapitalized character
    self.gsub(/(.)([A-Z])(?=[^A-Z])/,'\1_\2').
    # Insert underscore before capitalized letters that are preceded by an uncapitalized, non-underscore character
    gsub(/([^A-Z_])([A-Z])/,'\1_\2').
    # Replace whitespace with underscores
    gsub(/\s+/, '_').
    # Merge multiple underscores
    gsub(/_+/, '_').
    # Downcase
    downcase
  end
  
  def self.random(length = 10)
    alpha_num_chars = ("A"..."Z").to_a + ("a"..."z").to_a + ("0"..."9").to_a
    s=''
    length.times{ s << alpha_num_chars[rand(alpha_num_chars.length)] }
    s
  end
  
  ##### Adapted from Ramaze and Rack::Utils in order to not be dependent on those projects
  ESCAPE_HTML =
  {
    "&" => "&amp;",
    "<" => "&lt;",
    ">" => "&gt;",
    "'" => "&#x27;",
    '"' => "&quot;",
    "/" => "&#x2F;"
  }
  ESCAPE_HTML_PATTERN = Regexp.union(*ESCAPE_HTML.keys)
  
  # FIXME move this and associated constants to html_gui_builder
  def web_escape(which = :html)
    case which
    when :html
      to_s.gsub(ESCAPE_HTML_PATTERN) { |c| ESCAPE_HTML[c] }
    when :cgi
      ::URI.encode_www_form_component(self)
    when :uri
      ::URI.escape(self)
    else
      raise ArgumentError, "do not know how to escape '#{ which }'"
    end
  end
end
