class Array
  # Returns the classifier of the most generic type in an array
  # Another way to say this is that it returns the most specific classifier that
  # everything in the array is a kind_of?()
  def gb_most_generic_type
    classifier = self[0].class

    # Make sure that there is nothing in the array further up the inheritance
    self.values_at(1..(self.length - 1)).each do |obj|
      if obj.class != classifier
        objs_parents = obj.class.parents
        classifiers_parents = classifier.parents
        
        # Get the intersection of the parent classes
        matches = objs_parents & classifiers_parents
        
        
        if matches.empty?
          # Nothing in common. Return the most generic type
          return AlternateInheritance.top_level_ruby_const_exclusive || Object
        else
          # The closest common parent is the first element of the intersection array
          classifier = matches[0]
        end
      end
    end
    return classifier
  end
end