module Gui
  @options ||= {} # do not do lazy init
  
  def self.option_defaults
    {
      :default_view_name => :Details,
      :default_view_type => :Organizer,
      :root_dir      => File.expand_path('~/Prometheus'),
      :data_dir      => '<ROOT_DIR>/data/<SPEC_NAME>',
      :log_dir       => '<DATA_DIR>/logs/',
      :tempfile_dir  => '<DATA_DIR>/tmp/',
      :min_page_size => 1, # obviously this is not what you want but it keeps things from breaking
      :max_page_size => 1000
    }
  end
  
  # Get or set an option, substituting other option values as necessary
  def self.option(option_key, new_option_value = nil)
    opts = unevaluated_options
    unless new_option_value.nil? # Set new option value
      opts[option_key] = new_option_value
    else # Evaluate current option value
      option_value = opts[option_key]
      return option_value if !option_value.is_a?(String) || !option_value.index('<')
      opts.each do |key, val|
        option_value = option_value.gsub("<#{key.upcase}>") { Gui.option(key).to_s }
      end
      option_value
    end
  end
  
  # Return a hash of all options in an evaluated state
  def self.options
    interpreted_options = {}
    unevaluated_options.keys.each do |key|
      interpreted_options[key] = Gui.option(key)
    end
    interpreted_options
  end
  
  # Set options based on a hash of unevaluated options
  def self.unevaluated_options=(unevaluated_opts)
    @options = Gui.option_defaults.merge(unevaluated_opts)
  end
  
  # Return the unevaluated options
  def self.unevaluated_options
    unevaluated_options = {} if @options.empty? # ensures we have the defaults even if nobody every tried to set up any options
    @options
  end
  
  # Legacy setter to be compatible with older versions of gui_site
  def self.options=(unevaluated_opts)
    Kernel.warn "Warning: using deprecated Gui.options= options setter"
    self.unevaluated_options = unevaluated_opts
  end
  
  def self.application_modules
    @options[:application_modules] ||= []
  end
end