module Gui
  DEFAULT_ADMIN_RESTRICTED_CLASS_NAMES = [
    "Gui_Builder_Profile::User",
    "Gui_Builder_Profile::Person",
    "Gui_Builder_Profile::InvitationCode",
    "Gui_Builder_Profile::UserRole",
    "Gui_Builder_Profile::AdminRole",
    "Gui_Builder_Profile::ProjectOptions",
    "Gui_Builder_Profile::Perspective",
    "Gui_Builder_Profile::MultivocabularySubstitution",
   # Association classes
    "Gui_Builder_Profile::RolePermissions",
    "Gui_Builder_Profile::GrantedPermissions",
   # Join tables
    "Gui_Builder_Profile::User_UserRole_Roles",
    "Gui_Builder_Profile::InvitationCode_Perspective_Association",
    "Gui_Builder_Profile::Perspective_User_Association",
    "Gui_Builder_Profile::ProjectOptions_PasswordRequirement",
    "Gui_Builder_Profile::ProjectOptions_EmailRequirement",
   # Need finer grained way of handling these classes. It would be nice to use them elsewhere.
    "Gui_Builder_Profile::StringCondition",
    "Gui_Builder_Profile::RegularExpressionCondition",
    "Gui_Builder_Profile::Organization" # This should be removed since multitenancy has been removed, but for now it is still here.
  ]
  
  def self.admin_restricted_classes
    @admin_restricted_classes ||= []
  end
  
  def self.add_admin_restricted_class(klass)
    admin_restricted_classes << Object.const_get(klass.to_s) if Object.const_defined?(klass.to_s)
  end
  
  def self.setup_default_admin_restricted_classes
    DEFAULT_ADMIN_RESTRICTED_CLASS_NAMES.each { |k| add_admin_restricted_class(k) }
  end
  private_class_method :setup_default_admin_restricted_classes

end
# -----------------------------------------------------