# This is to aid in tracing the usage of all of the options that get passed around.  Hoping to refactor things and hoping this will help.
module Gui
  @@options_location_tracker = {}
  @@options_key_tracker = {}
  @@all_keys = []
  def self.options_tracker
    @@all_keys
  end
    
  def self.input_options(options)
    where1 = caller[0].sub(/.*projects.*lib\//, '')
    where2 = caller[1].sub(/.*projects.*lib\//, '')
    @@options_location_tracker[where1] ||= {}
    @@options_location_tracker[where1][:input] ||= {}
    @@options_location_tracker[where1][:input]['all']  ||= []
    @@options_location_tracker[where1][:input][where2] ||= []
    options.each_key { |k| @@options_location_tracker[where1][:input]['all'] << k unless @@options_location_tracker[where1][:input]['all'].include?(k) }
    options.each_key { |k| @@options_location_tracker[where1][:input][where2] << k unless @@options_location_tracker[where1][:input][where2].include?(k) }
    options.each_key { |k| _track_key(where1, :input, 'init obj') }
    options.each_key { |k| @@all_keys << k unless @@all_keys.include?(k) }
  end
  
  def self._track_option(where, tag, key, usage = nil)
    @@options_location_tracker[where] ||= {}
    @@options_location_tracker[where][tag] ||= []
    @@options_location_tracker[where][tag] << [key,usage].compact unless @@options_location_tracker[where][tag].include?([key,usage].compact)
    _track_key(where, tag, key, usage)
  end
  def self._track_key(where, tag, key, usage = nil)
    @@options_key_tracker[key] ||= {}
    @@options_key_tracker[key][tag] ||= []
    @@options_key_tracker[key][tag] << [where,usage] unless @@options_key_tracker[key][tag].include?([where,usage])
  end    
  
  def self.use_option(key, usage = nil)
    where = caller.first.sub(/.*projects.*lib\//, '')
    _track_option(where, :use, key, usage)
  end
  
  def self.use_options(keys, usage = nil)
    where = caller.first.sub(/.*projects.*lib\//, '')
    keys.each { |key| _track_option(where, :use, key, usage) }
  end
end
