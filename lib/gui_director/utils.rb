module Gui
  module_function
  
  # do we need to be concerned about ambiguous / duplicated routes that aren't really supposed to go to the same place?  How would we know or prevent such a thing?  
  def route_for(val)
    case val
    when String
      val.split('::').join('/')
    when Symbol
      route_for(val.to_s)
    when Class, Module
      route_for(val.name)
    else
      raise "Gui cannot create a route for #{val.class} - #{val.inspect}"
    end
  end
  
  def admin_roles
    Gui_Builder_Profile::UserRole.where(Sequel.ilike(:name, '%administrator%')).all
  end
  
  def admins
    admin_roles.map { |r| r.users }.flatten.uniq
  end
end
