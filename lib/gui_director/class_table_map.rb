# This functionality might fit more properly into SSA but in that case SSA would need to explicitly be told what the application module was and *probably* shouldn't be so opinionated about where the output file gets placed, whereas it doesn't seem so bad to choose where the file goes here inside the GUI framework.

require 'yaml'
module Gui
  def self.record_class_to_table_mapping(opts = {}) 
    application = Gui.option(:spec_module).to_const
    filename = File.expand_path("~/Prometheus/data/#{application::GEM_NAME}/class_table_map_#{application::VERSION}.yaml")
    use_existing_file = !(opts[:overwrite] || opts[:replace])
    return if File.exist?(filename) && use_existing_file
    hash = {}
    # Note -- This DOES NOT get ChangeTracker classes
    modules = application.top_level_modules.map(&:to_const)
    modules.each do |mod|
      mod.classes(no_imports:true).each do |klass|
        next unless klass < ORM_Class
        hash[klass.to_s] = klass.table_name rescue nil
      end
    end
    File.open(filename, "w+") do |file|
      file.puts YAML::dump(hash)
    end
  end

  def self.construct_table_migration_mapping v1, v2
    application = Gui.option(:spec_module).to_const
    source_hash = YAML::load(File.open(File.expand_path("~/Prometheus/data/#{application::GEM_NAME}/class_table_map_#{v1}.yaml")))
    target_hash = YAML::load(File.open(File.expand_path("~/Prometheus/data/#{application::GEM_NAME}/class_table_map_#{v2}.yaml")))
    source_only = source_hash.keys - target_hash.keys
    target_only = target_hash.keys - source_hash.keys
    puts "source_only: #{source_only.inspect}"
    puts "target_only: #{target_only.inspect}"
    mapping = {}
    source_hash.each do |k,v|
      mapping[k] = [v,target_hash[k]] if target_hash[k]
    end
    pp mapping
    map_file = File.expand_path("~/Prometheus/data/#{application::GEM_NAME}/table_mapping_#{v1}_to_#{v2}.yaml")
    File.open(map_file, "w+") { |file| file.puts YAML::dump(mapping) }
  end
end