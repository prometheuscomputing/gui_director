module Gui

  # the only API this has is various types of #render.  Director isn't the best name for it.
  class Director
    attr_accessor :builder, :domain_obj, :getter, :view_type, :view_name, :view_classifier, :options
    attr_reader   :user
    
    # FIXME don't pass a getter?  There should be ONLY ONE PLACE where that could possibly happen.  We already know it has been happening in gui.rb.  Either way, it should all be happening inside a Controller.
    def initialize(user:, builder:nil, obj:nil, getter:nil, view_type:nil, view_name:nil, view_classifier:nil, options:{})
      Gui.input_options(options) # debugging aid in tracking where and when options are used

      case builder
      when nil, :html_gui_builder
        require 'html_gui_builder'
        builder = HTMLBuilder.new
      end
      @builder         = builder
      # Use the default_domain_object (Gui::Home) if no domain object is specified
      # FIXME we want only one point where we are filling in a nil domain_obj with the homepage...and it probably should not be here.
      @domain_obj      = obj || Gui.loaded_spec.default_domain_object
      @getter          = getter
      @view_type       = view_type
      @view_name       = view_name
      @view_classifier = view_classifier
      @options         = options
      @user            = user
    end
  end  
end
