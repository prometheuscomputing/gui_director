module Gui
  class Director

    # Retrieves from the spec the view corresponding to the given domain_obj, getter, and view information.
    # Will also return the optional parent widget for the case of rendering simple widgets.
    #  - domain_obj may either be a domain object or the classifier for a domain object
    
    # Used exclusively in render.rb and in this file.
    def get_widget
      domain_obj_class = domain_obj.is_a?(Class) ? domain_obj : domain_obj.class
      # puts Rainbow('get_widget-->' + "(#{self.view_type.inspect}--#{self.view_name.inspect}--#{self.view_classifier.inspect}) domain_obj: #{domain_obj.class}[#{domain_obj&.id}]; getter: #{getter}").red
      if getter && domain_obj
        # FIXME will blow up if domain_obj isn't a ORM_Class
        # Get widget based on getter
        assoc_info = domain_obj_class.associations[getter.to_sym]
        return [get_view_widget(assoc_info), nil] if assoc_info
          
        attr_info = domain_obj_class.attributes[getter.to_sym]
        return get_simple_widget(attr_info) if attr_info
        
        # puts "Getter is #{getter} and domain_obj is #{domain_obj} and domain_obj_class is #{domain_obj_class}"
        raise "Invalid getter: #{getter.inspect} for object #{domain_obj.inspect}. Valid getters: #{(domain_obj.class.associations.keys + domain_obj.class.attributes.keys).sort.inspect}"
      else
        # Get widget for domain_obj
        if (domain_obj_class < Gui::AdHocPageObject || domain_obj_class < ORM_Class) && !domain_obj_class.complex_attribute?
          [get_view_widget, nil]
        else
          get_simple_widget(nil)
        end
      end
    end
    private :get_widget
    
    # Retrieves from the spec the view corresponding to the given domain_obj, getter, and view information.
    # only used in this file.  just a helper
    def get_view_widget(assoc_info = nil)#, parent_widget = nil)
      unless self.view_type # :Organizer or :Collection
        if assoc_info
          self.view_type = assoc_info[:type].to_s =~ /to_many$/ ? :Collection : :Organizer
        else
          self.view_type = Gui.loaded_spec.default_view_type
        end
      end
      unless self.view_name # :Summary or :Details
        self.view_name = view_type == :Collection ? :Summary : Gui.option(:default_view_name)
      end
      unless self.view_classifier
        if domain_obj
          if getter
            self.view_classifier = assoc_info[:class].to_const
          else
            self.view_classifier = domain_obj.is_a?(Class) ? domain_obj : domain_obj.class
          end
        else
          self.view_classifier = Gui.loaded_spec.default_classifier
        end
      end

      view = Gui.loaded_spec.retrieve_view(self.view_name.to_sym, self.view_classifier, self.view_type)
      if getter
        Gui.use_options([:object_view_name, :object_view_type, :disabled], 'ViewRef.new')
        view_ref_options = {
          :getter           => getter.to_s,
          :view_name        => self.view_name,
          :classifier       => self.view_classifier,
          :object_view_name => options[:object_view_name],
          :object_view_type => options[:object_view_type]
        }
        view_ref_options[:label]    = options[:label] if options[:label]
        view_ref_options[:disabled] = []
        # FIXME this will change or go away when policy is implemented properly
        view_ref_options[:disabled] << :all      if options[:disabled]
        view_ref_options[:disabled] << :create   if options[:creation_disabled]
        view_ref_options[:disabled] << :delete   if options[:deletion_disabled]
        view_ref_options[:disabled] << :remove   if options[:removal_disabled]
        view_ref_options[:disabled] << :add      if options[:addition_disabled]
        view_ref_options[:disabled] << :traverse if options[:traversal_disabled]
        view_ref = Gui::Widgets::ViewRef.new(view_ref_options)
        view_ref.view = view
        view_ref.orderable = assoc_info[:ordered] if assoc_info
        view_ref
      else
        view
      end
    end
    private :get_view_widget
    
    # only used in this file.  just a helper
    def get_simple_widget(attr_info = nil)#, parent_widget = nil)
      domain_obj_class = domain_obj.is_a?(Class) ? domain_obj : domain_obj.class
      if (domain_obj_class < ORM_Class || domain_obj_class < Gui::AdHocPageObject) && !domain_obj_class.complex_attribute?
        self.view_classifier ||= domain_obj_class
        parent_view_widget = get_view_widget
        child_widget = parent_view_widget.content.find { |w| w.getter.to_s == getter.to_s }
        [child_widget, parent_view_widget]
      else # domain_obj is an attribute
        Gui.use_option(:widget_type, 'Gui::Widgets.get_widget()')
        [Gui::Widgets.get_widget(options[:widget_type]), nil]
      end
    end
    private :get_simple_widget
    
  end
end
