module Gui
  class Director
    private        
    # build a root vertex (and all necessary sub or parent vertices) based on an object and a widget
    def build_vertex_tree(widget, parent_widget = nil)
      
      # This occurs when, e.g., rendering a collection on the page.
      parent_vertex = Gui::Vertex.new(user:user, widget:parent_widget, obj:domain_obj) if getter

      Gui.use_option(:page_state)
      page_state = options.delete(:page_state) # stop passing page_state around in options hash at this point!
      obj_state = page_state&.find do |obj_hash|
        obj_hash['data_classifier'] == domain_obj.class.name && (domain_obj.id.nil? || domain_obj.id.to_s == obj_hash['data_id'])
      end
      obj_state ||= {}
      
      # Create a root vertex
      # if getter is defined, then domain_obj is the parent's domain obj.
      root_vertex = Gui::Vertex.new(user:user, widget:widget, parent_vertex:parent_vertex, obj:((self.getter || widget&.getter)  ? nil : domain_obj), obj_state:obj_state, root_opts:options)

      Gui.use_options([:widget_label])

      # Override root widget label if options[:widget_label] is passed
      root_vertex.widget.label = options[:widget_label] if options[:widget_label]
      root_vertex
    end
  end
end
