module Gui
  class Director

    # Render a domain_obj. If none is passed to Direction#initialize, renders Gui.loaded_spec.default_domain_object
    
    # If getter is passed to Direction#initialize, then the rendering is performed on the result of calling the getter on the domain_obj.

    # This is the only public API for Director other than #new
    # Optional parent widget is used to create the parent vertex in the case of rendering simple widgets by themselves.
    def render(render_as = nil)
      case render_as
      when nil
        widget, parent_widget = get_widget
      when :collection, :selection
        return render_xction(render_as)
      else
        raise "#{self.class} does not know how to force rendering for #{force.inspect}"
      end
      raise "NO domain_obj in #{self.class}#render!\n#{caller.pretty_inspect}" unless domain_obj

      root_vertex = build_vertex_tree(widget, parent_widget)
      builder.render_vertex(root_vertex, options)
    end
    
    private
    def render_xction(render_as)
      # FIXME this is certainly still shaky and brittle.  The only thing sending this as a selection stems from a uniqueness popup.  There needs to be a uniform means of rendering a selection widget
      
      # Note about view_type:
      #   Currently, the html_gui_builder requires that a collection rendered based on an Organizer (such as showing
      #   possible associations for a many_to_one assocation) be based on an Organizer view type.
      #   This means that an Organizer widget must be used as the basis for rendering a collection.
      #   This leads to an Organizer having to have all the Collection functionality.
      #   TODO: Combine into a single View widget capable of displaying in singular and tabular modes.
      #         In this scenario, view_type becomes irrelevant and is removed. Tabular vs singular would
      #         be determined based on context (association type?, showing possible?).
      # NOTE: Forcing view to be Collection Summary.
      #       This changes unassociated Organizers from being rendered as CollectionFromOrganizers to being actual collections
      options[:collection_content] = true
      Gui.use_option(:collection_content, :set)
      self.view_type = :Collection
      self.view_name = :Summary unless self.view_name
      widget, parent_widget = get_widget
      case render_as
      when :collection
        widget.process_rendering_options(options)
      when :selection
        options[:selection] = true
        options[:expanded]  = true
      end
      root_vertex = build_vertex_tree(widget, parent_widget)
      builder.render_vertex(root_vertex, options)
    end
  end
end
