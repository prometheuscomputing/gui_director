module Gui
  
  ResolveError = Class.new(Exception)
  # ExpectedArrayContentError = Class.new(ResolveError)
  # UnexpectedNilError        = Class.new(ResolveError)
  
  def self.resolve_classifier(classifier, namespace = Object)
    return classifier if classifier.instance_of?(Class)
    begin
      classifier.split("::").inject(namespace) do |context, step| 
        step.empty? ? context : context.const_get(step)
      end
    rescue => err
      puts "#{classifier.inspect} - #{classifier.class}"
      if Object != namespace
        # Try it from the root
        resolve_classifier(classifier, Object)
      else
        sep = "\n\t"
        raise(ResolveError, "failed to resolve path #{classifier.inspect} in namespace #{namespace.inspect} - error was #{err.message} #{err.backtrace.join(sep)}")
      end
    end
  end
end
