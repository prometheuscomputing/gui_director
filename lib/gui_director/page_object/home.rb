module Gui
  class Home < AdHocPageObject
    def self.to_title
      Gui.option(:home_title) || super
    end    
  end
end
