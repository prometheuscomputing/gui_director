module Gui
  class AdHocPageObject
    include FakeDomainObj::InstanceMethods
    extend  FakeDomainObj::ClassMethods
    
    def page_title
      to_title
    end
  
    def to_title
      @title ||= self.class.to_title # Class#to_title defined in gui_director
    end
  
    private
    # FIXME implement usage of scope
    def _get_property(args_hash)
      scope  = args_hash[:scope]
      getter = args_hash[:getter]
      filter = args_hash[:filter] || {}
      limit  = args_hash[:limit]  || nil
      offset = args_hash[:offset] || 0
      order  = args_hash[:order]  || {}
      get_association_hash = args_hash[:return_associations_hash] || false
      send(getter.to_sym, filter, limit, offset, get_association_hash, order)
    end
    # alias_method :_get_property, :get_property
  
    class << self
  
      def associations
        @associations ||= {}
      end

      def attributes
        @attributes ||= {}
      end

      def properties
        attributes.merge(associations)
      end
    
      def get_info(getter)
        properties[getter.to_sym]
      end
    
      def add_methods(klass_sym, args = {})
  
        # FIXME use Appellation here.  It is going to give different results which could potentially break custom code.
        method_name = args[:getter] || klass_sym.to_s.split('::').last.downcase.pluralize

        # Indirection so the variable is correctly bound to the body of the define_method
        # klass_str = String.new(klass_sym.to_s)
        klass = klass_sym.to_s.to_const
  
        columns_that_must_be_nil_for_root = args[:ignore_when_present] || []
        # Hack to convert association names to columns #TODO: remove me and use associations
        columns_that_must_be_nil_for_root.map! { |nc| (nc.to_s << '_id').to_sym }

        root_filter = {}
        columns_that_must_be_nil_for_root.each { |col_name| root_filter[col_name] = nil }
  
        define_getter_method(method_name, klass, root_filter)
        define_count_method(method_name, klass)

        define_method(method_name + '_type') do
          klass.children + [klass]
        end
  
        define_method(method_name + '_unassociated') { |*optional_args| [] }
        define_method(method_name + '_unassociated_count') { |*optional_args| 0 }
        self.associations[method_name.to_sym] = {:type => :one_to_many, :getter => method_name.to_sym, :class => klass.name, :opp_getter => nil, :association => true}
      end
  
      # Results in method with what amounts to the following signature:
      #    <method_name>(custom_filter == {}, limit = nil, offset = 0, get_association_hash = false, order = {})
      def define_getter_method(method_name, klass, root_filter )
        define_method(method_name) do |*optional_args|
          custom_filter = optional_args.shift || {}
          limit         = optional_args.shift || nil
          return [] if limit == 0
          offset = optional_args.shift || 0
          get_association_hash = optional_args.shift || false
          order = optional_args.shift || {}
        
    
          # Custom filter is interpreted via LIKE
          # Root filter is interpreted via an exact match
          # Boolean storing whether or not there are any filters
          any_filters = custom_filter.any? || root_filter.any?
          if !any_filters && limit.nil? && offset == 0
            # All method automatically traverses subclasses
            ret = klass.all
          else
            klasses = [klass] + klass.children
      
            remaining_items  = limit
            remaining_offset = offset
            order_column     = order[:column]
            order_direction  = order[:descending_order]
      
            order_by_type = (order_column.to_s == 'sub_type' || order_column.to_s == 'type')
      
            # Handle 'type' filter
            # Set up filter_types and remove 'type' filter from custom_filter
            filter_type_names = custom_filter.map { |f| f.is_a?(Hash) ? f.delete('type') : nil }.flatten.compact
            filter_types = filter_type_names.map { |type_name| type_name.to_const }     
            custom_filter.delete_if { |f| f.is_a?(Hash) && f.empty? }

            #Derive the columns for the abstract class and add in the Sequel generated columns
            auto_generated_columns = [:id, :created_at, :updated_at]
            abstract_class_columns = klasses.first.properties.map do |k,v|
              property_type = v[:type]
              if property_type == :attribute
                v[:column]
              elsif property_type == :many_to_one && !v[:through]
                if v[:enumeration]
                  enum_class = v[:class].to_class
                  if enum_class.children.empty? && enum_class.parents.empty?
                    (k.to_s + '_id').to_sym
                  else
                    [(k.to_s + '_id').to_sym, (k.to_s + '_class').to_sym]
                  end
                elsif v[:opp_is_ordered]
                  [(k.to_s + '_id').to_sym, (k.to_s + '_position').to_sym]
                elsif v[:derived]
                  []
                else
                  (k.to_s + '_id').to_sym
                end
              elsif v[:holds_key]
                (k.to_s + '_id').to_sym
              end
            end.compact.flatten + auto_generated_columns
      
            ret = klasses.map do |k|            
              if (limit && remaining_items <= 0) || k.abstract? ||
                  (filter_types.any? && !filter_types.any? { |ft| k.nil? ? ft.nil? : (k == ft || k.parents.include?(ft)) })
                []
              else # Still need more items        
                # Set up query for this klass
                k_query = k
          
                #Splat arg must be here unless you use a diffent select wrapper method e.g. select{}
                k_query = k_query.select(*abstract_class_columns)

                k_query = k_query.select_append(Sequel.as(k.to_s, '_class_for_row_'))
                k_query = k_query.select_append(Sequel.as(:id, '_id_for_row_'))
                if order_column
                  k_query = k_query.select_append(Sequel.as(k.to_s, '_column_for_order_')) if order_by_type
                  k_query = k_query.select_append(Sequel.as(order_column, '_column_for_order_')) if !order_by_type
                  k_query = k_query.select_append(Sequel.as(Sequel.case([[{order_column=>nil},1]],0), '_column_for_sort_'))
                end
              
                k_query = k_query.filter(root_filter) unless root_filter.empty?
                # Custom filter can be either a hash or array here
                k_query = k_query.filter(custom_filter) unless custom_filter.empty?
                # TODO: benchmark times for doing count now vs. full query
                #       Full query is only needed if k_count < remaining_offset, but it may be worth it to go ahead and run
                k_count = k_query.count
                k_query
              end
            end
    
            ret = ret.flatten
            union_query = ret.shift
            ret.each { |query| union_query = union_query.union(query, :all=>true, :from_self=>false) }
      
            if order_column
              union_query = order_direction ? union_query.order(Sequel.desc(:_column_for_order_)).order_prepend(Sequel.desc(:_column_for_sort_)) : union_query.order(Sequel.asc(:_column_for_order_)).order_prepend(:_column_for_sort_)
            end
      
            #Limit the query based on the page offset and remaining itmes
            union_query = union_query.limit(limit, offset) if limit
      
            # union_query.row_proc = Proc.new{ |values| values[:_class_for_row_].to_const[values[:id]] }
            union_query.row_proc = Proc.new { |values| values[:_class_for_row_].to_const[values[:_id_for_row_]] }
      
            ret = union_query.all
          end
          ret = ret.map { |obj| {:to => obj} } if get_association_hash
          return ret
        end # define_method
      end
  
      def define_count_method(method_name, klass)
        define_method(method_name + '_count') do |*optional_args|
          filter = optional_args.shift || {}
          filter_type_names = filter.map { |f| f.is_a?(Hash) ? f.delete('type') : nil }.flatten.compact
          filter_types = filter_type_names.map(&:to_const)
          filter.delete_if { |f| f.is_a?(Hash) && f.empty? }
          klasses = klass.children + [klass]
          klasses.map do |k| 
            next 0 if k && k.abstract?
            next 0 if filter_types.any? && !filter_types.any? { |ft| k.nil? ? ft.nil? : (k == ft || k.parents.include?(ft)) }
            k.filter(filter).count
          end.inject(:+)
        end
      end
    
    end # class << self
  end
end
