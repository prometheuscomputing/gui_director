# Fake Domain Objects
module Gui
  module FakeDomainObj
    module InstanceMethods
      # Fake some Sequel / SSA methods
      def id; nil; end
      def compositions(*args); []; end
      def composer; []; end
      def save; end
      
      # Fake pending_ methods as needed
      def pending_association_additions(*args)       ; []; end
      def pending_association_removals(*args)        ; []; end
      def pending_association_addition_objects(*args); []; end
      def pending_association_removal_objects(*args) ; []; end
      # Fake hash index method
      def [](key); nil; end
    end
    
    module ClassMethods
      # Fake Sequel / SSA methods
      def association_class?; false; end
      def abstract?; false; end
      def complex_attribute?; false; end
      def parents(*args); []; end
      def immediate_parents; []; end
      def children(*args); []; end
      def immediate_children; []; end
      def compositions; []; end
      def composers; []; end
      def concrete_class; self; end
      def singleton?; false; end
      def interfaces; []; end
      def immediate_interfaces; []; end
      def implements?(*args); false; end
      def interface?; false; end
      def [](key); nil; end
      
    end # ClassMethods
  end # FakeDomainObj
end # Gui
