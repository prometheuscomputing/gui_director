require 'pundit'
require_dir(relative('policy'))
module Gui
  # This is somewhat dangerous.  Must ensure that this method isn't called for the first time until after the options have been parsed.
  @@policy_active = nil
  def self.policy_active?
    @@policy_active ||= !!option(:policy_active)
  end
  
  
  # These might just be temporary hacks until we can do this using Pundit...or they might be here forever...
  @@read_only_procs = []
  def self.read_only?
    @@read_only_procs.any? { |prok| prok.call }
  end
  
  def self.add_read_only_proc(prok)
    @@read_only_procs << prok
  end
end
