module Gui
  # TODO: Build system to send custom search types to gui_builder from app.  Store in hash, e.g. {'name_of_filter' => proc}
  # TODO: Put the widget specific functionality in the widgets instead of a case statement that basically switches on widget type, with the exception of custom defined filters
  
  # FIXME Write cukes for exclusion (i.e. "NOT") filters!!!
  def self.build_query_from_filter(search_filters)
    # puts Rainbow(search_filters.inspect).red
    search_filters = [search_filters].flatten # Dirty.  Should be able to rely on this being an array already.  Maybe we can??
    # switched to returning an empty array instead of an empty hash.  It seemed strange to return either an empty hash or an array (query).  Seems we should be returning a single type (or possibly nil)
    query = []
    return query if search_filters.nil? || search_filters.empty?
    search_filters.each do |search_filter|
      search_filter.each do |filter_type, filter_set|
        if filter_type == :not
          # then the filter_set is really {filter_type => filter_set}
          filter_set.each { |ftype, fset| query.concat(process_filter_set(ftype, fset, true)) }
        else
          query.concat(process_filter_set(filter_type, filter_set))
        end
      end
    end
    # puts Rainbow(search_filters.pretty_inspect).magenta
    # puts Rainbow(query.pretty_inspect).green
    query
  end
  
  def self.build_query_from_search_all_filter(filter)
    return [] if filter.nil? || filter.empty?
    search_all_filter = filter#.deep_symbolize_keys
    search_all_value = search_all_filter[:search_all_value]
    search_values = parse_search_all_value(search_all_value)
    and_query_set = []
    search_values.each do |search_terms|
      or_values = search_terms.split(" OR ")
      or_query_set = []
      or_values.each do |search_term|
        formatted_search_all_hash = tranform_search_all_filter(search_all_filter, search_term)
        query = build_query_from_filter([formatted_search_all_hash])
      
        #TODO pull out type hash from the array if there is one and add it back in after the OR and AND queries for all search_values
        #Right now this won't work since types are removed from the search pool instead of filtering based on type
        type_hash = query.select { |type_hash| type_hash.is_a?(Hash) && type_hash["type"] }
        query = query - type_hash
        
        #Also perform an ILike string search on date fields to catch extra searches
        date_hashes = formatted_search_all_hash[:date] || []
        extra_date_queries = date_hashes.map { |column, value| Sequel.ilike(column, "%#{value}%") }
        query = query + extra_date_queries
        or_query = Sequel.|(*query)
        or_query_set << or_query
      end
      #Or the all of the search_terms or_query sets before merging all of the queries together in the AND query set at the end
      and_query_set << Sequel.|(*or_query_set)
    end
    #The default filter is AND so there is no need to wrap these OR statements in an explicit Sequel.&  
    search_all_query = and_query_set

    #TODO Add type back into search if type searching gets changed to allow searching for type instead of limiting results by type
    # search_all_query = search_all_query + type_hash

    search_all_query
  end
  
  def self.process_filter_set(filter_type, filter_set, negation = false)
    query_additions = []
    filter_set.each do |field, value|
      # TODO would it be worthwhile to actually check and see if field is valid?  Thus preventing a possible Sequel error?
      # whitespace is implicit AND
      value = value.to_s
      value = value.gsub(" AND ", " ")
      # Do not split time filter types on whitespace ('2013/01/01 2:00:00' can't be split)
      unless [:timestamp, :time, :date].include?(filter_type)
        values = value.split(/(?<=[^( OR)]) (?=[^(OR )])/).reject { |v| v.strip.empty? }
      else
        values = [value]
      end
      
      values.each do |v|
        v.strip!
        # TODO: instead of checking for == "EMPTY" here, need to check on a per filter_type basis.
        #       Checking here prevents use of OR by the user (can't specify: Bob OR EMPTY)
        #
        #       Would also need to modify Symbol#ilike() to correctly interpret nil as an argument.
        #
        #       Would also be nice to implement NOT EMPTY, which is as simple as like '%_%' for strings but probably different for other types
        if v.is_a?(::String) && v == "EMPTY"
          # query << field.to_sym.ilike(nil, '')
          # NOTE: this is a non-standard Sequel filter. Sequel typically will ignore nil in an array of values, but a modication in SSA allows this usage.
          query_additions << {field.to_sym => [nil, '']}
        else
          filter_type = :sub_type if field == 'sub_type'
          case filter_type
          when :sub_type
            query_additions << {"type" => v.to_const}
          when :chemical_system
            v.split(/-| +/).each  do |component|
              query_additions << Sequel.ilike(field.to_sym, "#{component}-%", "%-#{component}-%", "%-#{component}", "#{component}")
            end
          when :disabled
            # anything marked as 'disabled' shouldn't have even made it this far but in case it did we just throw it out now
          when :boolean
            begin
              query_additions << {field.to_sym => search_string_to_boolean(v)}
            rescue ArgumentError
              # puts "Raised Argument Exception"
              next
            end
          when :timestamp, :time, :date
            case v
            when /^\d{4}$/ # A year (which is not handled correctly by chronic)
              # TODO Can use Sequel.extract again instead of ilike once Sequel is patched to properly qualify numeric expressions
              # block = {Sequel.extract(:year, field.to_sym) => v}
              # query << block
              query_additions << Sequel.ilike(field.to_sym, "%#{v}%")
            else
              ct = Chronic.parse(v, :guess => false)
              if ct.is_a?(::Time)
                query_additions << {field.to_sym => ct}
              elsif ct.is_a?(Chronic::Span)
                start_condition = Sequel.virtual_row {Sequel.identifier(field.to_sym) >= ct.begin}
                end_condition = Sequel.virtual_row {Sequel.identifier(field.to_sym) < ct.end}
                query_additions << Sequel.&(start_condition, end_condition)
              end
            end
          when :integer
            vors = v.split(/OR|,|\s+/).reject(&:empty?).map { |v_or| Integer(v_or.strip) }
            query_additions << {field.to_sym => vors}
            # query
          # when :hex_integer # NOTE this is no longer used anywhere
          #   # Exactly match the specified hex integer (or other base).
          #   # Partial matches are not currently supported since the value stored in the database
          #   # is decimal.
          #   decimal_int = Integer(v) rescue nil
          #   query_additions << {field.to_sym => decimal_int}
          when :case_sensitive_exact # NOTE never used unless by app specific configuration
            # vors = (v.split("OR").map { |v_or| "%#{v_or.strip}%" })
            vors = (v.split(" OR ").map { |v_or| "#{v_or.strip}" })
            query_additions << {field.to_sym => vors}
          when :case_insensitive_exact # NOTE never used unless by app specific configuration
            query_additions << Sequel.ilike(field.to_sym, "#{v}")
          else # Default to :case_insensitive_like
            vors = v.split(" OR ").map do |val|
              handled_val = handle_special_chars(val)
              "%#{handled_val.strip}%"
            end
            query_additions << Sequel.ilike(field.to_sym, *vors)
          end # case
        end # else
      end
    end
    query_additions = query_additions.map { |qa| Sequel.~(qa) } if negation
    puts Rainbow(query_additions.inspect).cyan
    query_additions
  end
  private_class_method :process_filter_set
  
  def self.handle_special_chars(val)
    val = val.gsub('_', '\_') # perserve literal underscore
    val = val.gsub('%', '\%') # perserve literal percent symbol
    val = val.gsub('*', '%') # UI wildcard character is asterisk, SQLite wildcard is %
    val
  end
  private_class_method :handle_special_chars
  
  #Used instead of the String class method to_boolean because it catches n,y,t,f and that is too generic for most searches
  def self.search_string_to_boolean(search_string)
    return nil if search_string.empty?
    return true if search_string =~ (/(true|yes|1)$/i)
    return false if search_string.empty? || search_string =~ (/(false|no|0)$/i)
    raise ArgumentError.new("invalid value for Boolean: \"#{search_string}\"")
  end
  private_class_method :search_string_to_boolean
  
  def self.tranform_search_all_filter(all_filter, search_value)
    columns = all_filter[:columns]
    search_value = search_value
    
    search_by_value_hash = {}
    columns.each do |name_key, column|
      column_name = column[:column_name]
      search_type = column[:search_type]
      search_by_value_hash[search_type] ||= {}
      search_by_value_hash[search_type][column_name] = search_value
    end
    
    #Bandaided Fix for all searching a type
    #Pass in the correct TYPE value to be used later if there was a matching object type to the searched value
    all_filter[:object_type] ? search_by_value_hash[:sub_type][:sub_type] = all_filter[:object_type] : search_by_value_hash.delete(:sub_type)
    search_by_value_hash
  end
  private_class_method :tranform_search_all_filter
  
  def self.parse_search_all_value(search_value)
    search_value = search_value.strip
    #TODO Add in different types of parsing eg. NOT
    search_value = search_value.gsub(" AND "," ")
    search_values = search_value.split(/(?<=[^( OR)]) (?=[^(OR )])/).reject { |v| v.strip.empty? }
    search_values
  end
  private_class_method :parse_search_all_value
end
