require 'yaml'
require 'sass'
require 'fileutils'

# TODO here is the point at which we could begin to swap out ORMs
require 'sequel'
ORM_Class = Sequel::Model

require 'common/constants'
require 'common/object_logger'
require 'common/multi_io'
require_relative 'admin_restricted_classes'
Infinity = 1.0/0 unless defined? Infinity # which it almost certainly has been in the generated models file

# Set encoding defaults
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

module Gui
  def self.configure_director(options = nil)
    Gui.unevaluated_options = options if options
    # Avoid deprecation warnings by adding after_initialize plugin before driver load
    ORM_Class.plugin :after_initialize

    # NOTE: Gui.option(:spec_name) currently includes '.rb' in the name. Should remove this probably, but not here.
    # For now, implementing this patch to remove.
    spec_name = option(:spec_name).sub(/\.rb$/, '')    
    
    # Load the generated gem based on the spec_name
    # --------------- Set up domain models ----------------    
    generated_models = [[spec_name, option(:spec_module)]]
    if option(:additional_specs)
      option(:additional_specs).each do |gem_name, module_name|
        generated_models << [gem_name, module_name]
      end
    end    
    self.spec_modules.concat(setup_generated_models(generated_models))
    self.spec_module = self.spec_modules.first

    # Instruct ChangeTracker to use the session's :full_name as the author of changes.
    ChangeTracker.author_key = :full_name

    # ensure useful directories exist
    [:data_dir, :log_dir, :tempfile_dir].each do |dir|
      val = option(dir)
      FileUtils.mkdir_p(val) if val && !File.exist?(val)
    end

    #setup redirect of all output to a log
    # app_log = File.new(File.join(option(:log_dir), "#{spec_name}.log"), 'a+')
    # app_log.sync = true
    # $stdout = MultiIO.new($stdout, app_log)
    # $stderr = MultiIO.new($stderr, app_log)

    # Set up object logger
    gui_director_log = File.new(File.join(option(:log_dir), 'gui_director.log'), 'a+')
    gui_director_log.sync = true
    object_logger(Logger.new(MultiIO.new($stdout, gui_director_log)))
    object_logger.log_warn! # Set log level to warn.

    if defined?(Sequel::SQLite::Database) && DB.is_a?(Sequel::SQLite::Database)
      DB.foreign_keys = false # Ensure foreign keys support is disabled
      # Turn off storing of timezones so SQLite's date/time functions work
      # See: http://stackoverflow.com/questions/12436259/ruby-sequel-filtering-by-date-year-and-attribute
      DB.use_timestamp_timezones = false
    end

    # Run schema extensions if available
    schema_ext_dir = option(:schema_extensions_dir)
    if schema_ext_dir && !schema_ext_dir.empty?
      puts "Applying schema migrations found at: #{schema_ext_dir}" if $verbose
      Sequel.extension(:migration)
      Sequel::Migrator.apply(DB, schema_ext_dir, nil)
    end

    # Require model extensions if available
    require option(:model_extensions) if option(:model_extensions) # We do not check for file existance. It's presumed to be on the load path, just like everything else.
    # Load internal model_extensions
    require 'gui_director/model_extensions'
    if defined?(Gui_Builder_Profile::UserRole)
      GuiDirector.commit_change_tracker do
        Gui_Builder_Profile::UserRole.admin
      end
    end
    record_class_to_table_mapping

    # -------------------- Set up spec --------------------
    load_specs
    setup_default_admin_restricted_classes
  end
  
  def self.load_specs
    specs = spec_modules.map(&:spec_file)
    custom_spec_dir_or_files = option(:custom_spec)
    if custom_spec_dir_or_files
      if custom_spec_dir_or_files =~ /ya?ml$/
        custom_specs = []
        yaml = YAML.load_file(custom_spec_dir_or_files)
        yaml.each do |directory, files|
          cs_dir = File.join(File.dirname(custom_spec_dir_or_files), directory)
          files.each do |f|
            # tolerant...
            spec = File.join(cs_dir, f)
            spec = File.join(cs_dir, f + '.rb') unless File.exist?(spec)
            spec = File.join(cs_dir, f + '_spec.rb') unless File.exist?(spec)
            custom_specs << spec
          end
        end
      elsif File.directory?(custom_spec_dir_or_files)
        custom_specs = Dir[File.join(custom_spec_dir_or_files, '**', '*')].reject { |f_or_d| File.directory?(f_or_d) }
      end
    else
      custom_specs = [custom_spec_dir_or_files]
    end
    specs += custom_specs if custom_specs&.any? # Load custom_spec if available
    specs += appended_additional_spec_files
    self.loaded_spec = GuiSpec.from_dsl_files(specs)
  end
  singleton_class.send(:alias_method, :reload_specs, :load_specs)
  
  # This method requires the necessary components from the generated models, adding in any extensions
  # It is a replacement for model/driver.rb from generated gems.
  # The primary model is the first model in the array (i.e. the model that maps to this project) -- I'm not sure how meaningful this really is but for now....
  def self.setup_generated_models(generated_models)
    gms = generated_models.map do |generated_gem_name, generated_module_name|
      require generated_gem_name
      generated_module = generated_module_name.to_const
      # TODO: Do not get module name by string manipulation
      module_name = generated_module_name.gsub(/Generated$/, '')
      [generated_module, module_name]
    end
    primary_module      = gms.first.first
    primary_module_name = gms.first.last

    option(:db_location, primary_module.db_location) unless option(:db_location)
    
    db_url = option(:db_url) if option(:db_url)
    if db_url.nil?
      db_file_name = option(:db_location)
      db_url = (RUBY_PLATFORM =~ /java/) ? "jdbc:sqlite:#{db_file_name}" : "sqlite://#{db_file_name}"
      option(:db_url, db_url)
    end

    require 'pg'        if db_url.index('postgres:')
    require 'sqlite3'   if db_url.index('sqlite:')
    require 'mysql2'    if db_url.index('mysql2:')
    require 'mysqlplus' if db_url.index('mysql:')

    require 'sequel_specific_associations'
    require 'sequel/extensions/inflector'
    require 'sequel_change_tracker'
    case option(:change_tracker_mode)
    when :enabled
      # default, do nothing
    when :limited
      ChangeTracker.limited_mode = true
    when :extra_limited
      ChangeTracker.extra_limited_mode = true
    end
    sequel_opts = {:pool_timeout => 30, :max_connections => 1}
    db = Sequel.connect(db_url, sequel_opts)
    # TODO: remove DB constant assignment
    # def_or_redef(:DB, db)
    Kernel.const_set(:DB, db)
    ORM_Class.db = db

    # require the models
    gms.each { |gm, _| require File.join(gm.model_dir, 'models.rb') }
    require option(:pre_schema_model_extensions) if option(:pre_schema_model_extensions)
  
    gms.each { |gm, _| require File.join(gm.model_dir, 'schema.rb') }
  
    top_level_modules = gms.map { |gm, _| gm.top_level_modules.map(&:to_const) }.flatten.uniq
  
    enums = []
  
    gms.each { |gm, _| require File.join(gm.model_dir, 'enumeration_instances.rb') }

    gms.map(&:first) # return the generated modules
  end
  private_class_method :setup_generated_models

  def self.orm_models_in(modules)
    models = []
    modules.each do |mod|
      consts = mod.constants.map { |constant| mod.const_get(constant) }
      classes, submodules = consts.partition { |c| c.is_a?(Class) }
      # Reject any non-modules from submodules
      submodules.reject! { |m| !m.is_a?(Module) }
      models.concat(classes.select { |c| c.ancestors.include?(ORM_Class) })
      models.concat(orm_models_in(submodules))
    end
    models
  end
  private_class_method :orm_models_in
  
  def self.loaded_spec
    @loaded_spec
  end
  
  def self.loaded_spec=(spec)
    @loaded_spec = spec
  end
  private_class_method :loaded_spec=
  
  def self.spec_module
    @spec_module
  end
  
  def self.spec_module=(mod)
    @spec_module = mod
  end
  private_class_method :spec_module=
  
  def self.spec_modules
    @spec_modules ||= []
    @spec_modules
  end
  
  # Never used by anything as of Nov 10, 2022
  def self.add_spec_module(mod)
    @spec_modules << mod
  end
  private_class_method :add_spec_module  
  
  def self.appended_additional_spec_files
    @appended_additional_spec_files ||= []
  end
  
  def self.append_file_to_spec(file)
    appended_additional_spec_files << file
  end
  
end
