# frozen_string_literal: true

# This file was generated programmatically on  2-MAY-2022 using the `package_gem` functionality of mmlite.

Gem::Specification.new do |s|
  s.name        = "gui_director"
  s.version     = "7.2.0"
  s.authors     = ["Samuel Dana", "Ben Dana", "Michael Faughn"]
  s.email       = ["s.dana@prometheuscomputing.com", "michael.faughn@nist.gov"]
  s.homepage    = "https://gitlab.com/prometheuscomputing/gui_director"
  s.summary     = "Displays Gui's based on a given reference implementation."
  s.description = "AJAX web application for viewing and editing data structured by a UML model."
  s.licenses    = ['MPL-2.0']
  s.files = [
      "LICENSE",
      "README.rdoc",
      "lib/gui_director/ruby_extensions/string.rb",
      "lib/gui_director/ruby_extensions/array.rb",
      "lib/gui_director/ruby_extensions/class.rb",
      "lib/gui_director/ruby_extensions/hash.rb",
      "lib/gui_director/ruby_extensions/regexp.rb",
      "lib/gui_director/ruby_extensions/object.rb",
      "lib/gui_director/meta_info.rb",
      "lib/gui_director/model_extensions/richtext.rb",
      "lib/gui_director/model_extensions/orm_instance.rb",
      "lib/gui_director/model_extensions/regex_condition.rb",
      "lib/gui_director/model_extensions/binary_data.rb",
      "lib/gui_director/model_extensions/multivocab_substitution.rb",
      "lib/gui_director/model_extensions/utils.rb",
      "lib/gui_director/model_extensions/file.rb",
      "lib/gui_director/model_extensions/code.rb",
      "lib/gui_director/model_extensions/perspective.rb",
      "lib/gui_director/model_extensions/string_condition.rb",
      "lib/gui_director/model_extensions/project_options.rb",
      "lib/gui_director/model_extensions/user.rb",
      "lib/gui_director/model_extensions/user_role.rb",
      "lib/gui_director/model_extensions.rb",
      "lib/gui_director/errors.rb",
      "lib/gui_director/track_option_use.rb",
      "lib/gui_director/director.rb",
      "lib/gui_director/latex.rb",
      "lib/gui_director/director/director.rb",
      "lib/gui_director/director/get_widget.rb",
      "lib/gui_director/director/build_root_vertex.rb",
      "lib/gui_director/director/render.rb",
      "lib/gui_director/director_config.rb",
      "lib/gui_director/search.rb",
      "lib/gui_director/class_table_map.rb",
      "lib/gui_director/page_object/ad_hoc_page_object.rb",
      "lib/gui_director/page_object/setup.rb",
      "lib/gui_director/page_object/fake_domain_obj.rb",
      "lib/gui_director/page_object/home.rb",
      "lib/gui_director/resolvable.rb",
      "lib/gui_director/utils.rb",
      "lib/gui_director/gui_builder.rb",
      "lib/gui_director/policy.rb",
      "lib/gui_director/vertex/counter.rb",
      "lib/gui_director/vertex/info.rb",
      "lib/gui_director/vertex/widget_methods.rb",
      "lib/gui_director/vertex/vertex.rb",
      "lib/gui_director/vertex/access.rb",
      "lib/gui_director/vertex/pending_associations.rb",
      "lib/gui_director/admin_restricted_classes.rb",
      "lib/gui_director/options.rb",
      "lib/gui_director/policy/policy_methods.rb",
      "lib/gui_director/policy/bootstrap_policy.rb",
      "lib/gui_director/policy/base_policy.rb",
      "lib/gui_director/policy/cache.rb",
      "lib/gui_director/policy/creation.rb",
      "lib/gui_director.rb"
  ]
  s.test_files = [
      "spec/spec_helper.rb",
      "spec/lodepath.yml",
      "spec/gui_director_spec.rb"
  ]
  s.add_dependency("lodepath", "~> 0.1")
  s.add_dependency("common", "~> 1.12")
  s.add_dependency("sequel_specific_associations", "~> 11.0")
  s.add_dependency("sequel_change_tracker", "~> 2.15")
  s.add_dependency("gui_spec", "~> 1.6")
  s.add_dependency("pundit", "~> 2.1")
  s.add_dependency("concurrent-ruby", "~> 1.1")
  s.add_dependency("activesupport", "~> 7.0")
  s.add_dependency("multivocabulary", "~> 0.1")
  s.add_dependency("titleize", "~> 1.4")
  s.add_dependency("blankslate", "~> 3.1")
  s.add_dependency("mime-types", "~> 3.3")
  s.add_dependency("chronic", "~> 0.10")
  s.add_dependency("rainbow", "~> 3.0")
  s.add_development_dependency("rspec", "~> 3.0")
  s.add_development_dependency("car_example_generated", "~> 2.7")
  s.add_development_dependency("car_example", "~> 1.27")
end
