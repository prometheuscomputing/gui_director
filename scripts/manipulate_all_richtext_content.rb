def ct
  ChangeTracker.start unless ChangeTracker.started?
  ChangeTracker.commit
  ChangeTracker.start
end

ChangeTracker.start unless ChangeTracker.started?
models = ORM_Class.children.reject { |model| model.to_s =~ /ChangeTracker/ }
models.each do |model|
  puts "Running Through Model: #{model}"
  rt_content_getters = model.db_schema.select { |key, value| key.to_s =~ /_content$/ && value[:type] == :string }.keys
  if rt_content_getters
    begin
      instances = model.all
      instances.each do |instance| 
      rt_content_getters.each do |getter|
        if content = instance.send(getter)
          new_content = content.sub(/\A(\r|\n)+/m, '').sub(/(\r|\n)+\z/m, '').gsub(/\r/, '').force_encoding('utf-8')
          setter = (getter.to_s + "=").to_sym
          instance.send(setter, new_content)
          instance.save
          ct
        end
      end
    end
    rescue Sequel::DatabaseError => e
      puts e.message
    end
  end
end